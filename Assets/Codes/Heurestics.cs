﻿using UnityEngine;

public static class Heuristics
{
    // implementation for integer based Manhattan Distance
    /*public static int ManhattanDistance(int x1, int x2, int y1, int y2)
    {
        return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
    }*/

    // implementation for floating-point  Manhattan Distance
    public static float ManhattanDistance(Vector2 start, Vector2 to /*float x1, float x2, float y1, float y2*/)
    {
        return Mathf.Abs(start.x - to.x) + Mathf.Abs(start.y - to.y);
    }

    // implementation for integer based Euclidean Distance
    public static int EuclideanDistance(int x1, int x2, int y1, int y2)
    {
        int square = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        return square;
    }

    // implementation for floating-point EuclideanDistance
    public static float EuclideanDistance(float x1, float x2, float y1, float y2)
    {
        float square = (x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2);
        return square;
    }

    // implementation for integer based Chebyshev Distance
    public static int ChebyshevDistance(int dx, int dy)
    {
        // not quite sure if the math is correct here
        return 1 * (dx + dy) + (1 - 2 * 1) * (dx - dy);
    }

    // implementation for floating-point Chebyshev Distance
    public static float ChebyshevDistance(float dx, float dy)
    {
        // not quite sure if the math is correct here
        return 1 * (dx + dy) + (1 - 2 * 1) * (dx - dy);
    }
}