﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System;

public class FileSelector : MonoBehaviour
{
    [SerializeField]
    private UnityEngine.UI.InputField m_inputField;

    [SerializeField]
    private GameObject m_fileBrowser;

    [SerializeField]
    private VerticalLayoutGroup m_layoutGroup;

    [SerializeField]
    private Scrollbar m_verticalScroll;

    [SerializeField]
    private UnityEngine.UI.Button m_folderButton;

    [SerializeField]
    private UnityEngine.UI.Button m_upperFolderButton;

    private string m_curFolderPath;


    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        //Init the file browser, show root folder.
#if UNITY_STANDALONE
        m_curFolderPath = @"C:\";
#elif UNITY_ANDROID
        m_curFolderPath = @"storage/";
#endif
        UpdateFolderView();
    }

    private void UpdateFolderView()
    {
        print(m_curFolderPath);
        m_inputField.text = m_curFolderPath;
        //refresh folder view, show subfolders of the current folder

        //clear view
        for(int i = 0; i < m_layoutGroup.transform.childCount; i++)
        {
            Destroy(m_layoutGroup.transform.GetChild(i).gameObject);
        }
        m_upperFolderButton.enabled = false;
        //get new subfolders
        StartCoroutine("GetSubFolders");
        StartCoroutine("GetTextFilesInFolder");
    }

    private IEnumerator GetSubFolders()
    {
        string[] subFolders = Directory.GetDirectories(m_curFolderPath);

        int index = 0;
        while(index < subFolders.Length)
        {
            print(subFolders[index]);
            CreateFolderButton(subFolders[index]);
            index++;
            yield return null;
        }
        
        yield return null;

        m_verticalScroll.value = 1;
        m_upperFolderButton.enabled = true;
    }

    private IEnumerator GetTextFilesInFolder()
    {
        List<string> txtFiles = new List<string>();

        txtFiles.AddRange( Directory.GetFiles(m_curFolderPath, "*.txt") );

        int index = 0;
        while (index < txtFiles.Count)
        {
            print(txtFiles[index]);
            CreateFileButton(txtFiles[index]);
            index++;
            yield return null;
        }

        yield return null;

        m_verticalScroll.value = 1;
        m_upperFolderButton.enabled = true;
    }

    public void MoveUp()
    {
        m_curFolderPath = Directory.GetParent(m_curFolderPath).FullName;
        UpdateFolderView();
    }

    private void CreateFolderButton(string folderPath)
    {
        GameObject button = GameObject.Instantiate(m_folderButton.gameObject);
        button.transform.SetParent(m_layoutGroup.transform);
        button.GetComponent<RectTransform>().localScale = Vector3.one;
        button.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = Path.GetFileName(folderPath);
        button.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => { m_curFolderPath = folderPath; UpdateFolderView(); });
    }

    private void CreateFileButton(string filePath)
    {
        GameObject button = GameObject.Instantiate(m_folderButton.gameObject);
        button.transform.SetParent(m_layoutGroup.transform);
        button.GetComponent<RectTransform>().localScale = Vector3.one;
        button.transform.GetChild(0).GetComponent<UnityEngine.UI.Text>().text = Path.GetFileName(filePath);
        button.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => { m_inputField.text = filePath; });
    }
}
