﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyTimer : MonoBehaviour
{
    public float timer;

    void FixedUpdate()
    {
        if (timer <= 0)
        {
            Destroy(gameObject);
        }

        timer -= Time.deltaTime;
    }
}
