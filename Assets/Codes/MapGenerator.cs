﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Threading;

public class MapGenerator : MonoBehaviour {

    public List<GameObject> wallPrefab;
    public GameObject chestSPrefab;
    public GameObject chestMPrefab;
    public GameObject chestLPrefab;
    public GameObject chestCPrefab;
    public GameObject hiddenTilePrefab;
    public GameObject stairsPrefab;
    public List<GameObject> doorPrefab;
    public List<GameObject> backgroundPrefab;

    public List<AudioClip> bgMusic;

    public List<GridSpace> gSpace;
    public GridManager grm;

    public bool aMapLoadComplete = false;
    private string androidText = "";

    // If game starts from main menu this gets changed to desired level
    // That means that if any scene containing this code is the starting scene
    // then map named here will be generated
    public static string levelToGenerate = "TestMap.txt";

    public IEnumerator AndroidDownload(string url)
    {
        Debug.Log(url);
        WWW www = new WWW(url);
        yield return www;
        androidText = www.text;
        GenerateMap();
        aMapLoadComplete = true;
    }

    public void AGenerateMap()
    {
        if(levelToGenerate == "Map1.txt" || levelToGenerate == "Map2.txt" || levelToGenerate == "TestMap.txt")
            StartCoroutine(AndroidDownload("jar:file:///" + Application.dataPath + "!/assets/Maps/" + levelToGenerate));
        else
            StartCoroutine(AndroidDownload("file:///" + levelToGenerate));
    }

    public void GenerateMap ()
    {
        //set different music to lvl 2
        if(levelToGenerate == "Map2.txt")
        {
            FindObjectOfType<AudioSource>().clip = bgMusic[1];
            FindObjectOfType<AudioSource>().Play();
        }

        List<string> rows = new List<string>();
        List<string> style = new List<string>();

#if UNITY_ANDROID
        //StartCoroutine(AndroidDownload(/*"jar:file://" +*/ "file://" + Application.dataPath + /*"!/assets*/@"/StreamingAssets" + @"/Maps/" + levelToGenerate));
        string[] sArr = androidText.Split('\n');
        rows.AddRange(sArr);
#else
        // Finding the map file
        //TODO: CLEAN THIS SHIT
        string dataPath = Application.dataPath;
        string[] ps = dataPath.Split('/');
        string actualPath = "";

        for (int i = 0; i < ps.Length-1; i++)
        {
            actualPath += ps[i] + '\\';
        }        
        string text = System.IO.File.ReadAllText(actualPath + @"\Maps\" + levelToGenerate);

        //parses text by linebreaks
        string[] sArr = text.Split('\n');
        rows.AddRange(sArr);
#endif
        // Removes style rows
        for (int i = 0; i < rows.Count; i++)
        {
            if (rows[i][0] == '#')
            {
                style.Add(rows[i]);
                rows.Remove(rows[i]);
                i--;
            }
        }
        // Prints rows for debuging
        for (int i = 0; i < rows.Count; i++)
        {
            Debug.Log(rows[i] + " " + rows[i].Length);
        }
        int xCord = 0, yCord = 0;
        for (int i = 0; i < style.Count; i++)
        {
            Debug.Log(style[i]);
        }

        // Sends map size to gridManager
        grm.XAmount = rows.Count;
        grm.YAmount = rows[0].Length / 2;

        rows.Reverse();
        
        // Because entities cannot be spawned during row creation
        // they will be stored in these lists to be spawned after rows creation
        List<Entity> PlayerSpawningCharacters = new List<Entity>();
        List<Vector2> PlayerSpawningLocations = new List<Vector2>();
        int playerIndex = 0;
        List<Entity> EnemySpawningCharacters = new List<Entity>();
        List<Vector2> EnemySpawningLocations = new List<Vector2>();
        int enemyIndex = 0;

        int tileSet=0;
        int background=0;
        if (style.Count > 0)
        {
            for (int i = 0; i < style.Count; i++)
            {
                if (i == 0)
                    tileSet = int.Parse(style[i].Substring(5, 1));
                else if (i == 1)
                    background = int.Parse(style[i].Substring(5, 1));
            }
        }

        GameObject.Instantiate(backgroundPrefab[background], new Vector3(20, 0, 13), Quaternion.Euler(90, 0, 0));

        // Spawning tiles to Y-axel
        for (int i = 0; i < rows.Count; i++)
        {
            yCord += 1;
            List<GridSpace> g = new List<GridSpace>();

            // Spawning tiles to X-axel
            for (int j = 0; j < rows[i].Length - 1; j++)
            {
                string selector = "";
                // Creates selector which is used in following switch case
                // If this for loop is at the end of rows[i] it skips creating selector because
                // selector needs to be two characters long
                if (j+1 != rows[i].Length)
                    selector = rows[i].Substring(j, 2);
                
                GridSpace gs;
                AIController ac = FindObjectOfType<AIController>();
                switch (selector)
                {
                    case "--": // Floor
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        g.Add(gs);
                        break;

                    case "Ww": // Wall
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.canBeWalked = false;
                        gs.isWall = true;
                        GameObject.Instantiate(wallPrefab[tileSet], gs.transform.position + Vector3.up * 0.5f, Quaternion.identity);
                        //go.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);

                        g.Add(gs);
                        break;

                    case "D1": // Vertical door
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        gs.isDoor = true;
                        
                        gs.doorObj = GameObject.Instantiate(doorPrefab[tileSet], gs.transform.position + Vector3.up * 0.5f, Quaternion.Euler(0, -90, 0));

                        foreach (MeshRenderer mr in gs.doorObj.GetComponentsInChildren<MeshRenderer>())
                            mr.enabled = false;
                        gs.wallObj = GameObject.Instantiate(wallPrefab[tileSet], gs.transform.position + Vector3.up * 0.5f, Quaternion.identity);

                        g.Add(gs);
                        break;

                    case "D2": // Horizontal door
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        gs.isDoor = true;

                        gs.doorObj = GameObject.Instantiate(doorPrefab[tileSet], gs.transform.position + Vector3.up * 0.5f, Quaternion.identity);

                        foreach (MeshRenderer mr in gs.doorObj.GetComponentsInChildren<MeshRenderer>())
                            mr.enabled = false;
                        gs.wallObj = GameObject.Instantiate(wallPrefab[tileSet], gs.transform.position + Vector3.up * 0.5f, Quaternion.identity);

                        g.Add(gs);
                        break;

                    case "Dd": // Hidden door used to block alleys from been seen instantly
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        gs.isDoor = true;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        g.Add(gs);
                        break;

                    case "C1": // Chest 1, Small size
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        gs.isChest = true;
                        gs.chestType = GridSpace.ChestType.Small;
                        GameObject temp = GameObject.Instantiate(chestSPrefab, gs.transform.position + Vector3.up * 0.5f, Quaternion.Euler(-90, 0, 0));
                        gs.chest = temp.GetComponent<Chest>();

                        foreach (MeshRenderer mr in gs.chest.GetComponentsInChildren<MeshRenderer>())
                            mr.enabled = false;

                        g.Add(gs);
                        break;

                    case "C2": // Chest 2, medium size
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        gs.chestType = GridSpace.ChestType.Medium;
                        gs.isChest = true;
                        GameObject temp1 = GameObject.Instantiate(chestMPrefab, gs.transform.position + Vector3.up * 0.5f, Quaternion.identity);
                        gs.chest = temp1.GetComponent<Chest>();

                        foreach (MeshRenderer mr in gs.chest.GetComponentsInChildren<MeshRenderer>())
                            mr.enabled = false;

                        g.Add(gs);
                        break;

                    case "C3": // Chest 3, Large size
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        gs.chestType = GridSpace.ChestType.Large;
                        gs.isChest = true;
                        GameObject temp2 = GameObject.Instantiate(chestLPrefab, gs.transform.position + Vector3.up * 0.5f, Quaternion.identity);
                        gs.chest = temp2.GetComponent<Chest>();

                        foreach (MeshRenderer mr in gs.chest.GetComponentsInChildren<MeshRenderer>())
                            mr.enabled = false;

                        g.Add(gs);
                        break;

                    case "Cc": // Chest C, Critical 
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        gs.chestType = GridSpace.ChestType.Objective;
                        gs.isChest = true;
                        GameObject temp3 = GameObject.Instantiate(chestCPrefab, gs.transform.position + Vector3.up * 0.5f, Quaternion.identity);
                        gs.chest = temp3.GetComponent<Chest>();

                        foreach (MeshRenderer mr in gs.chest.GetComponentsInChildren<MeshRenderer>())
                            mr.enabled = false;

                        g.Add(gs);
                        break;

                    case "E1": // Enemy 1: Skeleton
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        g.Add(gs);

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        EnemySpawningCharacters.Add(ac.enemyPrefab[0]);
                        EnemySpawningLocations.Add(new Vector2((float)gs.xPosition, (float)gs.yPosition));
                        enemyIndex++;
                        break;

                    case "E2": // Enemy 2: Orc
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        g.Add(gs);

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        EnemySpawningCharacters.Add(ac.enemyPrefab[1]);
                        EnemySpawningLocations.Add(new Vector2((float)gs.xPosition, (float)gs.yPosition));
                        enemyIndex++;
                        break;

                    case "E3": // Enemy 3: Zombie
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        g.Add(gs);

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        EnemySpawningCharacters.Add(ac.enemyPrefab[2]);
                        EnemySpawningLocations.Add(new Vector2((float)gs.xPosition, (float)gs.yPosition));
                        enemyIndex++;
                        break;

                    case "E4": // Enemy 4: dark knight
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        g.Add(gs);

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        EnemySpawningCharacters.Add(ac.enemyPrefab[3]);
                        EnemySpawningLocations.Add(new Vector2((float)gs.xPosition, (float)gs.yPosition));
                        enemyIndex++;
                        break;

                    case "o1": // Object stairs
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        gs.canBeWalked = false;
                        GameObject go = GameObject.Instantiate(stairsPrefab, gs.transform.position + Vector3.up * 0.5f, Quaternion.identity);
                        go.GetComponent<Stairs>().space = gs;
                        g.Add(gs);
                        break;

                    case "P1": // Player character 1
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        g.Add(gs);

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        PlayerController pc1 = FindObjectOfType<PlayerController>();
                        PlayerSpawningCharacters.Add(pc1.playerPrefabs[0]);
                        PlayerSpawningLocations.Add(new Vector2((float)gs.xPosition, (float)gs.yPosition));
                        playerIndex++;
                        break;

                    case "P2": // Player character 2
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        g.Add(gs);

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        PlayerController pc2 = FindObjectOfType<PlayerController>();
                        PlayerSpawningCharacters.Add(pc2.playerPrefabs[1]);
                        PlayerSpawningLocations.Add(new Vector2((float)gs.xPosition, (float)gs.yPosition));
                        playerIndex++;
                        break;

                    case "P3": // Player character 3
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        g.Add(gs);

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        PlayerController pc3 = FindObjectOfType<PlayerController>();
                        PlayerSpawningCharacters.Add(pc3.playerPrefabs[2]);
                        PlayerSpawningLocations.Add(new Vector2((float)gs.xPosition, (float)gs.yPosition));
                        playerIndex++;
                        break;

                    case "P4": // Player character 4
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;
                        g.Add(gs);

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        PlayerController pc4 = FindObjectOfType<PlayerController>();
                        PlayerSpawningCharacters.Add(pc4.playerPrefabs[3]);
                        PlayerSpawningLocations.Add(new Vector2((float)gs.xPosition, (float)gs.yPosition));
                        playerIndex++;
                        break;

                    case "S0": // Search event 1  !!NOT USED!!
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        g.Add(gs);
                        break;

                    case "T1": // Trap 1  !!NOT USED!!
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        g.Add(gs);
                        break;

                    case "__": // Empty 
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        gs.canBeWalked = false;
                        gs.isEmpty = true;

                        gs.GetComponent<MeshRenderer>().enabled = false;
                        g.Add(gs);
                        break;

                    case "D_": // Empty hidden door, used to block clearing of fog from empty tiles
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.isHidden = true;
                        gs.isDoor = true;
                        gs.hiddenFog = GameObject.Instantiate(hiddenTilePrefab, gs.transform.position + Vector3.up * 2.5f, Quaternion.identity);

                        gs.canBeWalked = false;
                        gs.isEmpty = true;

                        gs.GetComponent<MeshRenderer>().enabled = false;
                        g.Add(gs);
                        break;

                    default:
                        // If code doesn't know the object user is trying to spawn
                        // it spawns a green empty tile for debugging
                        gs = GameObject.Instantiate(gSpace[tileSet], new Vector3(xCord, 0, yCord), Quaternion.Euler(-90, 0, 0)) as GridSpace;
                        gs.xPosition = yCord - 1;
                        gs.yPosition = xCord;

                        gs.GetComponent<Renderer>().material.color = Color.red;
                        g.Add(gs);
                        break;

                }
                xCord += 1;
                j++;
            }
            grm.grid.Add(g);
            xCord = 0;
        }

        for(int i = 0; i < playerIndex; i++)
        {
            PlayerController pc1 = FindObjectOfType<PlayerController>();
            Entity P1 = GameManager.GM.SpawnEntity(PlayerSpawningCharacters[i], (int)PlayerSpawningLocations[i].x, (int)PlayerSpawningLocations[i].y);
            P1.eOwner = EntityOwner.Player;
            pc1.playerEntities.Add(P1);
        }
        for (int i = 0; i < enemyIndex; i++)
        {
            AIController ac1 = FindObjectOfType<AIController>();
            Entity E1 = GameManager.GM.SpawnEntity(EnemySpawningCharacters[i], (int)EnemySpawningLocations[i].x, (int)EnemySpawningLocations[i].y);
            E1.eOwner = EntityOwner.AI;

            //disable enemies if they are in rooms(hidden tiles)
            if (GameManager.GM.gridManager.getSpace((int)E1.xPosInGrid, (int)E1.yPosInGrid).isHidden)
            {
                E1.isDisabled = true;
                //E1.GetComponent<MeshRenderer>().enabled = false;
                foreach (MeshRenderer mr in E1.GetComponentsInChildren<MeshRenderer>())
                    mr.enabled = false;
            }

            ac1.enemies.Add(E1);
        }
    }
}
