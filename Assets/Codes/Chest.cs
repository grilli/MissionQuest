﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour
{
    public GridSpace.ChestType chestType;
    public void CheckForChest(Entity InvOwner)
    {
        GridManager grm = FindObjectOfType<GridManager>();
        //List<GridSpace> neighbouringChests = new List<GridSpace>();
        GridSpace ch;

        //one node down
        ch = grm.getSpace(InvOwner.xPosInGrid, InvOwner.yPosInGrid - 1);
        if (ch.isChest)
            GameManager.GM.chestCanBeOpened = true;
            //neighbouringChests.Add(ch);

        //one node up
        ch = grm.getSpace(InvOwner.xPosInGrid, InvOwner.yPosInGrid + 1);
        if (ch.isChest)
            GameManager.GM.chestCanBeOpened = true;
            //neighbouringChests.Add(ch);

        //one node to left
        ch = grm.getSpace(InvOwner.xPosInGrid - 1, InvOwner.yPosInGrid);
        if (ch.isChest)
            GameManager.GM.chestCanBeOpened = true;
            //neighbouringChests.Add(ch);

        //one node to right
        ch = grm.getSpace(InvOwner.xPosInGrid + 1, InvOwner.yPosInGrid);
        if (ch.isChest)
            GameManager.GM.chestCanBeOpened = true;
            //neighbouringChests.Add(ch);
    }

    public void chestLoot(GridSpace gs)
    {
        int num = 0;
        int num2 = 0;
        switch(chestType)
        {
            case GridSpace.ChestType.Small:
                //lesser healing
                FindObjectOfType<PlayerController>().inventory.AddItem(new HealthPotion(ItemDatabase.IDB.GetConsumableWithName("Lesser Healing Potion"), 3));
                num = 100;
                num2 = Random.Range(1, (num + 1));
                if (num2 > 0 && num2 < 51)
                {
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Armor(ItemDatabase.IDB.GetArmorWithName("Leather Armor")));
                }          
                else if (num2 > 50 && num2 < 71)
                {
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Shield(ItemDatabase.IDB.GetShieldWithName("Buckler Shield")));
                }
                PlayerController.addScrore(5);
                SoundManager.PlayASource("OpenChest");
                break;

            case GridSpace.ChestType.Medium:
                num = 100;
                num2 = Random.Range(1, (num+1));
                if (num2 > 0 && num2 < 76)
                {
                    //lesser healing
                    FindObjectOfType<PlayerController>().inventory.AddItem(new HealthPotion(ItemDatabase.IDB.GetConsumableWithName("Lesser Healing Potion"), 3));
                }
                else
                {
                    //greater healing
                    FindObjectOfType<PlayerController>().inventory.AddItem(new HealthPotion(ItemDatabase.IDB.GetConsumableWithName("Greater Healing Potion"), 7));
                }
                num = 168;
                num2 = Random.Range(1, (num+1));
                if (num2>0 && num2<44)
                {
                    //Lesser rune
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Weapon(ItemDatabase.IDB.GetWeaponWithName("Lesser Rune")));
                }
                else if (num2 > 43 && num2 < 57)
                {
                    //Normal rune
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Weapon(ItemDatabase.IDB.GetWeaponWithName("Normal Rune")));
                }
                else if (num2 > 56 && num2 < 100)
                {
                    //Leather armor
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Armor(ItemDatabase.IDB.GetArmorWithName("Leather Armor")));
                }
                else if (num2 > 99 && num2 < 113)
                {
                    //Chainmail Armor
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Armor(ItemDatabase.IDB.GetArmorWithName("Chainmail Armor")));
                }
                else if (num2 > 112 && num2 < 156)
                {
                    //buckler shield
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Shield(ItemDatabase.IDB.GetShieldWithName("Buckler Shield")));
                }
                else 
                {
                    //kite shield
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Shield(ItemDatabase.IDB.GetShieldWithName("Kite Shield")));
                }
                num = 100;
                num2 = Random.Range(1, (num + 1));
                if(num2>0 && num2<11)
                {
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Revive(ItemDatabase.IDB.GetConsumableWithName("Revive Poultice")));
                }
                PlayerController.addScrore(10);
                SoundManager.PlayASource("OpenChest");
                break;

            case GridSpace.ChestType.Large:
                num = 100;
                num2 = Random.Range(1, (num + 1));
                if (num2 > 0 && num2 < 21)
                {
                    //Greater healing
                    FindObjectOfType<PlayerController>().inventory.AddItem(new HealthPotion(ItemDatabase.IDB.GetConsumableWithName("Greater Healing Potion"), 7));
                }
                else if (num2 > 20 && num2 < 41)
                {
                    //Revive
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Revive(ItemDatabase.IDB.GetConsumableWithName("Revive Poultice")));
                }
                else if (num2 > 40 && num2 < 61)
                {
                    //Teleport
                    FindObjectOfType<PlayerController>().inventory.AddItem(new TeleportationStaff(ItemDatabase.IDB.GetConsumableWithName("Teleportation Staff")));
                }
                else if (num2 > 60 && num2 < 81)
                {
                    //Speed
                    FindObjectOfType<PlayerController>().inventory.AddItem(new SpeedPotion(ItemDatabase.IDB.GetConsumableWithName("Speed Potion")));
                }
                else
                {
                    //Stoneskin
                    FindObjectOfType<PlayerController>().inventory.AddItem(new StoneskinPotion(ItemDatabase.IDB.GetConsumableWithName("Stoneskin Potion")));
                }
                num = 99;
                num2 = Random.Range(1, (num + 1));
                if (num2 > 0 && num2 < 34)
                {
                    //Greater rune
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Weapon(ItemDatabase.IDB.GetWeaponWithName("Greater Rune")));
                }
                else if (num2 > 33 && num2 < 67)
                {
                    //Plate armor
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Armor(ItemDatabase.IDB.GetArmorWithName("Plate Armor")));
                }
                else 
                {
                    //Tower shield
                    FindObjectOfType<PlayerController>().inventory.AddItem(new Shield(ItemDatabase.IDB.GetShieldWithName("Tower Shield")));
                }
                PlayerController.addScrore(25);
                SoundManager.PlayASource("OpenChest");
                break;

            case GridSpace.ChestType.Objective:
                GameManager.GM.hasFinalKey = true;
                FindObjectOfType<Stairs>().space.canBeWalked = true;
                ScreenLog.PrintToLog("Key to the stairs found.");
                FindObjectOfType<PlayerController>().inventory.AddItem(new Consumable(ItemDatabase.IDB.GetConsumableWithName("Key")));
                //objective
                PlayerController.addScrore(500);
                SoundManager.PlayASource("KeyCollected");
                break;
        }

        gs.chest = null;
        gs.isChest = false;

        Destroy(gameObject);
    }  
}
