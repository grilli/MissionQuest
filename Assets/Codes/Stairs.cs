﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Stairs : MonoBehaviour
{
    public GridSpace space;
    public bool isvictory;
    void OnTriggerEnter(Collider col)
    {
        if(!isvictory && col.GetComponent<Entity>().eOwner == EntityOwner.Player && GameManager.GM.hasFinalKey)
        {
            Debug.Log("Goal reached");
            ScreenLog.PrintToLog("YOU'RE WINNER!");
            GameManager.GM.canAct = false;
            GameManager.GM.victoryScreen.SetActive(true);
            GameManager.GM.victoryScreen.transform.GetChild(1).GetComponent<Text>().text = "Your Score: " + PlayerController.score.ToString() + "!";
            isvictory = true;
            SoundManager.PlayASource("Victory");
        }
    }
}
