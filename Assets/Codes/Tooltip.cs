﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Tooltip : MonoBehaviour
{
    public Text tooltipText;

    private Inventory inv;

    private void Awake()
    {
        inv = FindObjectOfType<Inventory>();
        gameObject.SetActive(false);
    }

	public void ShowTooltip(int i)
    {
        if(inv.ItemsInInventory[i].iName != "" && inv.ItemsInInventory[i].image != null)
        {
            tooltipText.text = "<color=#00ff00ff>" + inv.ItemsInInventory[i].iName + "</color>\n";
            tooltipText.text += inv.ItemsInInventory[i].desc;
            gameObject.SetActive(true);
        }
    }

    public void MoveToSlotPosition(Transform slotPos)
    {
        transform.position = new Vector3(slotPos.position.x + ((RectTransform)slotPos).sizeDelta.x * slotPos.root.GetComponent<Canvas>().scaleFactor * 2, slotPos.position.y);
    }

    public void HideTooltip()
    {
        gameObject.SetActive(false);
    }
}
