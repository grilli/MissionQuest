﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item
{
    public string iName;
    public ItemType iType;
    public Sprite image;
    public string desc;
 
    public virtual void UseItem()
    {

    }
}

public enum ItemType
{
    Armor,
    Weapon,
    Shield,
    Consumable,
    Key
}

[System.Serializable]
public class Armor : Item
{
    public int defenceDiceIncrease;
    public int healthIncrease;
    public int slowAmount;

    public Armor(int defenceDiceIncrease, string iName, Sprite image, string desc, int healthIncrease, int slowAmount)
    {
        this.defenceDiceIncrease = defenceDiceIncrease;
        this.iName = iName;
        this.image = image;
        this.desc = desc;
        this.healthIncrease = healthIncrease;
        this.slowAmount = slowAmount;
    }
    public override void UseItem()
    {
        base.UseItem();

        //equip armor if can
        GameObject.FindObjectOfType<Inventory>().EquipItem(ItemType.Armor, this);
        SoundManager.PlayASource("Equip");
    }
    public Armor(Armor copyFrom)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;

        defenceDiceIncrease = copyFrom.defenceDiceIncrease;
        healthIncrease = copyFrom.healthIncrease;
        slowAmount = copyFrom.slowAmount;
    }
}

[System.Serializable]
public class Shield : Item
{
    public int defenceDiceIncrease;
    public int slowAmount;

    public Shield(int defenceDiceIncrease, string iName, Sprite image, string desc, int slowAmount)
    {
        this.defenceDiceIncrease = defenceDiceIncrease;
        this.iName = iName;
        this.image = image;
        this.desc = desc;
        this.slowAmount = slowAmount;
    }
    public override void UseItem()
    {
        base.UseItem();

        //equip shield if can
        GameObject.FindObjectOfType<Inventory>().EquipItem(ItemType.Shield, this);
        SoundManager.PlayASource("Equip");
    }
    public Shield(Shield copyFrom)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;

        defenceDiceIncrease = copyFrom.defenceDiceIncrease;
        slowAmount = copyFrom.slowAmount;
    }
}

[System.Serializable]
public class Weapon : Item
{
    public int attackDiceIncrease;

    public Weapon(int attackDiceIncrease, string iName, Sprite image, string desc)
    {
        this.attackDiceIncrease = attackDiceIncrease;
        this.iName = iName;
        this.image = image;
        this.desc = desc;
    }

    public override void UseItem()
    {
        base.UseItem();

        //equip weapon if can
        GameObject.FindObjectOfType<Inventory>().EquipItem(ItemType.Weapon, this);
        SoundManager.PlayASource("Equip");
    }

    public Weapon(Weapon copyFrom)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;

        attackDiceIncrease = copyFrom.attackDiceIncrease;
    }
}

[System.Serializable]
public class Consumable : Item
{
    public override void UseItem()
    {
        base.UseItem();
        //use consumable
    }
    public Consumable(Consumable copyFrom)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;
    }

    public Consumable() { }
}

[System.Serializable]
public class HealthPotion : Consumable
{
    public int healingAmount = 5;
    public override void UseItem()
    {
        base.UseItem();
        GameObject.FindObjectOfType<PlayerController>().currentActingEntity.stats.AddHealth(healingAmount);
        if(healingAmount==10)
            SoundManager.PlayASource("GreaterHealing");
        else
            SoundManager.PlayASource("LesserHealing");
    }
    public HealthPotion(Consumable copyFrom, int hAmount)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;
        healingAmount = hAmount;
    }
}

[System.Serializable]
public class Revive : Consumable
{
    public override void UseItem()
    {
        base.UseItem();
        Entity e = GameObject.FindObjectOfType<PlayerController>().currentActingEntity;
        e.stats.AddHealth(e.stats.maxHealth);
        e.isDead = false;
        MeshRenderer[] mrs = e.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mr in mrs)
            mr.material.color = Color.white;
        if (e.GetComponent<MeshRenderer>())
            e.GetComponent<MeshRenderer>().material.color = Color.white;
        SoundManager.PlayASource("Revive");
    }
    public Revive(Consumable copyFrom)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;
    }
}

[System.Serializable]
public class SpeedPotion : Consumable
{
    public override void UseItem()
    {
        base.UseItem();

        //3 rounds of speedbuff
        GameObject.FindObjectOfType<PlayerController>().currentActingEntity.speedPotionUptime += 4;
        SoundManager.PlayASource("Speed");
    }
    public SpeedPotion(Consumable copyFrom)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;
    }
}

[System.Serializable]
public class StoneskinPotion : Consumable
{
    public override void UseItem()
    {
        base.UseItem();

        //2 rounds of defence buff
        GameObject.FindObjectOfType<PlayerController>().currentActingEntity.stoneskinPotionUptime += 3;
        SoundManager.PlayASource("StoneSkin");
    }
    public StoneskinPotion(Consumable copyFrom)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;
    }
}

[System.Serializable]
public class TeleportationStaff : Consumable
{
    public override void UseItem()
    {
        base.UseItem();

        if(GameManager.GM.currentTurnType == Turn.Player)
        {
            GridSpace gs = GameManager.GM.gridManager.getSpace(Random.Range(0, GameManager.GM.gridManager.XAmount), Random.Range(0, GameManager.GM.gridManager.YAmount));
            while (!gs.canBeWalked || gs.isHidden || gs.isDoor || gs.isChest)
                gs = GameManager.GM.gridManager.getSpace(Random.Range(0, GameManager.GM.gridManager.XAmount), Random.Range(0, GameManager.GM.gridManager.YAmount));

            GameManager.GM.currentMovingEntity.SetEntityPosition(gs.xPosition, gs.yPosition);
            SoundManager.PlayASource("Teleport");
        }
    }
    public TeleportationStaff(Consumable copyFrom)
    {
        iName = copyFrom.iName;
        iType = copyFrom.iType;
        image = copyFrom.image;
        desc = copyFrom.desc;
    }
}

