﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class Stats : MonoBehaviour
{
    public int level;

    public int health;
    public int maxHealth;

    public int attackDiceAmount;
    public int defenceDiceAmount;

    public int movementDiceAmount;
    private int defaultMovementDiceAmount;

    public Armor armor = null;
    public Weapon weapon = null;
    public Shield shield = null;

    public int attackRange;

    private Entity e;

    void Awake()
    {
        e = GetComponent<Entity>();
        defaultMovementDiceAmount = movementDiceAmount;
    }

    public void TakeDamage(int dmg)
    {
       /* switch(e.eName)
        {
            case "Warrior":
                SoundManager.PlayASource("GetHit");
                break;
            case "Hunter":
                SoundManager.PlayASource("GetHit");
                break;
            case "Mage":
                SoundManager.PlayASource("GetHit");
                break;
            case "Rogue":
                SoundManager.PlayASource("GetHit");
                break;
            case "Skeleton":
                SoundManager.PlayASource("Skeleton");
                break;
            case "Zombie":
                SoundManager.PlayASource("Zombie");
                break;
            case "Orc":
                SoundManager.PlayASource("Orc");
                break; 
        }*/



        int calcDmg = 0;
        calcDmg = health - dmg;

        if (calcDmg <= 0)
        {
            health = 0;
            Die();
        }
        else
        {
            health = calcDmg;
        }

    }

    public void AdjustStats(int aDice, int dDice, int health, int slow, ItemType iType)
    {
        switch(iType)
        {
            case ItemType.Weapon:
                if (weapon != null)
                {
                    attackDiceAmount -= weapon.attackDiceIncrease;
                }
                attackDiceAmount += aDice;
                break;
            case ItemType.Armor:
                if (armor != null)
                {
                    defenceDiceAmount -= armor.defenceDiceIncrease;
                    maxHealth -= armor.healthIncrease;
                    this.health -= armor.healthIncrease;

                    //if we are locked at 1 moveroll and shield doesnt affect out speed
                    if(armor.slowAmount > 0 && movementDiceAmount == 1 && shield.slowAmount == 0)
                    {
                        movementDiceAmount += armor.slowAmount - 1;
                    }
                    //if we are below normal movement amount, but our shield does affect speed
                    else if (armor.slowAmount > 0 && movementDiceAmount < defaultMovementDiceAmount && shield.slowAmount > 0)
                    {
                        if (movementDiceAmount + armor.slowAmount - shield.slowAmount > 1)
                            movementDiceAmount += armor.slowAmount;
                        else
                            movementDiceAmount = 1;
                    }
                }
                defenceDiceAmount += dDice;
                maxHealth += health;
                this.health += health;

                if (movementDiceAmount - slow < 1)
                    movementDiceAmount = 1;
                else
                    movementDiceAmount -= slow;

                break;
            case ItemType.Shield:
                if(shield != null)
                {
                    defenceDiceAmount -= shield.defenceDiceIncrease;

                    //if we are locked at 1 moveroll and armor doesnt affect out speed
                    if (shield.slowAmount > 0 && movementDiceAmount == 1 && armor.slowAmount == 0)
                    {
                        movementDiceAmount += shield.slowAmount - 1;
                    }
                    //if we are below normal movement amount, but our armor does affect speed
                    else if (shield.slowAmount > 0 && movementDiceAmount < defaultMovementDiceAmount && armor.slowAmount > 0)
                    {
                        if (movementDiceAmount + shield.slowAmount - armor.slowAmount > 1)
                            movementDiceAmount += shield.slowAmount;
                        else
                            movementDiceAmount = 1;
                    }

                }

                defenceDiceAmount += dDice;

                if (movementDiceAmount - slow < 1)
                    movementDiceAmount = 1;
                else
                    movementDiceAmount -= slow;
                break;
            default:
                break;
        }
    }

    public void AddHealth(int hp)
    {
        health += hp;
        if (health > maxHealth)
            health = maxHealth;

        Canvas cv = Instantiate(GameManager.GM.worldNumber, e.transform.position + Vector3.up * 1.5f, Quaternion.Euler(90, 0, 0));
        cv.transform.GetChild(1).GetComponent<Text>().text = hp.ToString();
        cv.transform.GetChild(1).GetComponent<Text>().color = Color.green;
    }

    public void Die()
    {
        if(e.eOwner == EntityOwner.AI)
        {
            switch (e.eName)
            {
                case "Skeleton":
                    SoundManager.PlayASource("SkeletonDeath");
                    break;
                case "Zombie":
                    SoundManager.PlayASource("ZombieDeath");
                    break;
                case "Orc":
                    SoundManager.PlayASource("DeathOrc");
                    break;
            }



            PlayerController.addScrore(e.scorevalue);
            GridSpace gs = GameManager.GM.gridManager.getSpace(e.xPosInGrid, e.yPosInGrid);
            gs.hasEntity = false;
            gs.canBeWalked = true;
            gs.charOnTile = null;

            ScreenLog.PrintToLog("<color=#ff0000ff>" + e.eName + "</color> dies.");
            FindObjectOfType<AIController>().enemies.Remove(e);
            Destroy(e.gameObject);
        }
        else
        {
            switch (e.eName)
            {
                case "Warrior":
                    SoundManager.PlayASource("DeathWarrior");
                    break;
                case "Hunter":
                    SoundManager.PlayASource("DeathHunter");
                    break;
                case "Mage":
                    SoundManager.PlayASource("DeathMage");
                    break;
                case "Rogue":
                    SoundManager.PlayASource("RogueDeath");
                    break;          
            }




            //logic when player dies
            e.isDead = true;
            MeshRenderer[] mrs = e.GetComponentsInChildren<MeshRenderer>();
            foreach (MeshRenderer mr in mrs)
                mr.material.color = Color.gray;
            if(e.GetComponent<MeshRenderer>())
                e.GetComponent<MeshRenderer>().material.color = Color.gray;
            ScreenLog.PrintToLog("<color=#00ff00ff>" + e.eName + "</color> dies.");
        }
    }
}
