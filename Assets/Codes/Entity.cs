﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Entity : MonoBehaviour
{
    public int xPosInGrid, yPosInGrid;
    public Action action;
    public EntityOwner eOwner;
    public Sprite image;
    public string eName;
    public Stats stats;
    public int scorevalue;
    public bool isDisabled = false;
    public AttackType attackType;

    public int speedPotionUptime;
    public int stoneskinPotionUptime;

    //only used by player entities
    public bool isDead = false;
    
    private void Awake()
    {
        action = GetComponent<Action>();
        stats = GetComponent<Stats>();
    }

    public void Start()
    {
        //action.PerformAction(ActionType.Move);
    }

    public Vector2 GetEntityPosition()
    {
        return new Vector2(xPosInGrid, yPosInGrid);
    }

    ///Set position of entity in x,y grid
    public void SetEntityPosition(int xPosInGrid, int yPosInGrid)
    {
        //Not tested, might break the game, maybe?
        GameManager.GM.gridManager.getSpace(this.xPosInGrid, this.yPosInGrid).charOnTile = null;
        GameManager.GM.gridManager.getSpace(this.xPosInGrid, this.yPosInGrid).hasEntity = false;
        GameManager.GM.gridManager.getSpace(this.xPosInGrid, this.yPosInGrid).canBeWalked = true;

        //whenever we set postion we need to take account the fact that we have flipped the x and y axis when we create the map
        transform.position = new Vector3(yPosInGrid, 1, xPosInGrid + 1);

        this.xPosInGrid = xPosInGrid;
        this.yPosInGrid = yPosInGrid;

        GameManager.GM.gridManager.getSpace(xPosInGrid, yPosInGrid).charOnTile = this;
        GameManager.GM.gridManager.getSpace(xPosInGrid, yPosInGrid).hasEntity = true;

        //open door zulul
        if (eOwner == EntityOwner.Player && GameManager.GM.gridManager.getSpace(xPosInGrid, yPosInGrid).isDoor)
        {
            GridSpace door = GameManager.GM.gridManager.getSpace(xPosInGrid, yPosInGrid);
            //AStarPathfinding asp = FindObjectOfType<AStarPathfinding>();
            //List<GridSpace> neighbours = asp.FindValidFourNeighbours(door);
            if (door.doorObj != null)
                door.doorObj.gameObject.SetActive(false);
            ClearHidden(door);
        }

        if (eOwner == EntityOwner.Player && GameManager.GM.gridManager.getSpace(xPosInGrid, yPosInGrid).isChest)
        {
            GameManager.GM.gridManager.getSpace(xPosInGrid, yPosInGrid).chest.chestLoot(GameManager.GM.gridManager.getSpace(xPosInGrid, yPosInGrid));
        }

    }

    public void ClearHidden(GridSpace gs)
    {
        StartCoroutine(ClearNeighbours(gs));
    }

    ///this is a recursive function that clears the room from shadowtiles
    private IEnumerator ClearNeighbours(GridSpace gs)
    {
        List<GridSpace> neighbours = FindObjectOfType<AStarPathfinding>().FindValidFourNeighbours(gs);

        //We clear hidden door and nearby doors before looping
        if (gs.isDoor && gs.hiddenFog != null)
        {
            gs.hiddenFog.SetActive(false);
        }
        for (int i = 0; i < neighbours.Count; i++)
        {
            if (neighbours[i].isDoor && neighbours[i].hiddenFog != null) { 
                neighbours[i].hiddenFog.SetActive(false);
            }
        }
        for (int i = 0; i < neighbours.Count; i++)
        {
            bool wasHidden = false;

            if (neighbours[i].isHidden)
            {
                wasHidden = true;
                neighbours[i].isHidden = false;
                neighbours[i].hiddenFog.SetActive(false);

                if (neighbours[i].hasEntity)
                {
                    neighbours[i].charOnTile.isDisabled = false;
                    foreach(MeshRenderer child in neighbours[i].charOnTile.transform.GetComponentsInChildren<MeshRenderer>())
                        child.enabled = true;
                }
                else if (neighbours[i].isChest)
                {
                    foreach (MeshRenderer child in neighbours[i].chest.GetComponentsInChildren<MeshRenderer>())
                        child.enabled = true;
                }
            }
            if (neighbours[i].isDoor && neighbours[i].wallObj != null)
            {
                foreach (MeshRenderer child in neighbours[i].wallObj.GetComponentsInChildren<MeshRenderer>())
                    child.enabled = false;
                //neighbours[i].wallObj.SetActive(false);
                foreach (MeshRenderer child in neighbours[i].doorObj.GetComponentsInChildren<MeshRenderer>())
                    child.enabled = true;
            }
            if (!neighbours[i].isWall && !neighbours[i].isDoor && /*!neighbours[i].isEmpty &&*/ wasHidden)
                StartCoroutine(ClearNeighbours(neighbours[i]));
        }
        yield return null;
    }

    ///Show inspect screen
    private void OnMouseDown()
    {
        //dont do anything if UI is ontop of this object
        if (EventSystem.current.IsPointerOverGameObject())
            return;
        
            // Melee attack
            // Checks if it's currently player turn and if player can attack
            if (GameManager.GM.currentTurnType == Turn.Player && GameManager.GM.playerCanAttack && GameManager.GM.playerMeleeAttack && !FindObjectOfType<PlayerController>().hasAttacked && GameManager.GM.canAct)
        {
            Entity entity = GameManager.GM.currentMovingEntity;
            bool isNeighbour = false;
            List<GridSpace> gsl = GameManager.GM.gridManager.GetComponent<AStarPathfinding>().FindValidFourNeighbours(FindObjectOfType<GridManager>().getSpace(entity.xPosInGrid, entity.yPosInGrid));
            for (int i = 0; i < gsl.Count; i++)
            {
                if (gsl[i].charOnTile == this)
                    isNeighbour = true;
            }

            if (entity != this && isNeighbour)
            {
                List<GridSpace> gs = new List<GridSpace>();
                gs.Add(FindObjectOfType<GridManager>().getSpace(this.xPosInGrid, this.yPosInGrid));
                GameManager.GM.gridManager.GetComponent<AStarPathfinding>().movementRotation(gs, 0);
                entity.action.Attack(entity, this, true);
                FindObjectOfType<PlayerController>().hasAttacked = true;
                SoundManager.PlayASource("SwordHit");
            }
        }
        // Ranged attack
        else if (GameManager.GM.currentTurnType == Turn.Player && GameManager.GM.playerCanAttack && GameManager.GM.playerRangedAttack && !FindObjectOfType<PlayerController>().hasAttacked && GameManager.GM.canAct)
        {
            Entity entity = GameManager.GM.currentMovingEntity;
            // Checks if target tile is in range
            // TODO: use pathfinding instead
            if ((int)GameManager.GM.gridManager.getDistanceBetweenEntities(entity, this) <= entity.stats.attackRange)
            {
                Debug.Log((int)GameManager.GM.gridManager.getDistanceBetweenEntities(entity, this));
                
                if (entity != this)
                {
                    entity.action.Attack(entity, this, false);
                    FindObjectOfType<PlayerController>().hasAttacked = true;
                    SoundManager.PlayASource("HunterAttack");
                }
            }
        }
        // Magic attack
        else if (GameManager.GM.currentTurnType == Turn.Player && GameManager.GM.playerCanAttack && GameManager.GM.playerMagicAttack && !FindObjectOfType<PlayerController>().hasAttacked && GameManager.GM.canAct)
        {
            Entity entity = GameManager.GM.currentMovingEntity;
            // Checks if target tile is in range
            // TODO: use pathfinding instead
            //if ((Mathf.Abs(entity.GetEntityPosition().x - this.xPosInGrid) + Mathf.Abs(entity.GetEntityPosition().y - this.yPosInGrid)) <= entity.stats.attackRange)
            if ((int)GameManager.GM.gridManager.getDistanceBetweenEntities(entity, this) <= entity.stats.attackRange)
            {
                Debug.Log((int)GameManager.GM.gridManager.getDistanceBetweenEntities(entity, this));

                List<GridSpace> gsl = new List<GridSpace>();
                gsl = GameManager.GM.gridManager.GetComponent<AStarPathfinding>().FindValidFourNeighbours(FindObjectOfType<GridManager>().getSpace(this.xPosInGrid, this.yPosInGrid));
                gsl.Add(FindObjectOfType<GridManager>().getSpace(this.xPosInGrid, this.yPosInGrid));

                if (entity != this)
                {
                    for (int i = 0; i < gsl.Count; i++)
                    {
                        if (gsl[i].hasEntity)
                        {
                            if (!gsl[i].charOnTile.isDead)
                            {
                                entity.action.Attack(entity, gsl[i].charOnTile, false);
                                SoundManager.PlayASource("FireSpell");
                            }
                        }
                        GameObject.Instantiate(GameManager.GM.fireParticle, gsl[i].transform.position + Vector3.up * 2f, Quaternion.identity);

                    }
                    FindObjectOfType<PlayerController>().hasAttacked = true;
                }
            }
        } // InspectScreen
        else if (!GameManager.GM.isc.gameObject.activeSelf && !isDisabled)
        {
            GameManager.GM.isc.UpdateInspectScreen(this);
            GameManager.GM.isc.gameObject.SetActive(true);
            GameManager.GM.canAct = false;
        }
    }
}

public enum EntityOwner
{
    Player,
    AI
}

public enum AttackType
{
    Melee,
    Ranged,
    Magic,
    BackStab
}