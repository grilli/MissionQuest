﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScreenLog : MonoBehaviour
{
    private static ScreenLog instance;

    public Text logText;
    public int maxLines;


    private void Awake()
    {
        instance = this;
    }

    ///Prints text into new row on the log and deletes old lines if linecount is > "maxLines"
    public static void PrintToLog(string text)
    {
        string[] rows = instance.logText.text.Split('\n');
        if(rows.Length > instance.maxLines)
        {
            for (int i = 0; i < rows.Length; i++)
                rows[i] = (i == rows.Length - 1) ? text : rows[i + 1];

            instance.logText.text = "";

            for (int i = 0; i < rows.Length; i++)
                instance.logText.text += (i == rows.Length - 1) ? rows[i] : rows[i] + '\n';
        }
        else
        {
            instance.logText.text += "\n";
            instance.logText.text += text;
        }
    }	
}
