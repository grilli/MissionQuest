﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    public List<Weapon> weapons = new List<Weapon>();
    public List<Armor> armors = new List<Armor>();
    public List<Shield> shields = new List<Shield>();
    public List<Consumable> consumables = new List<Consumable>();

    public static ItemDatabase IDB;

    private void Awake()
    {
        IDB = this;
    }

    public Weapon GetWeaponWithName(string n)
    {
        foreach(Weapon w in weapons)
        {
            if (w.iName == n)
                return w;
        }

        return null;
    }

    public Consumable GetConsumableWithName(string n)
    {
        foreach (Consumable c in consumables)
        {
            if (c.iName == n)
                return c;
        }

        return null;
    }

    public Armor GetArmorWithName(string n)
    {
        foreach (Armor a in armors)
        {
            if (a.iName == n)
                return a;
        }

        return null;
    }

    public Shield GetShieldWithName(string n)
    {
        foreach (Shield s in shields)
        {
            if (s.iName == n)
                return s;
        }

        return null;
    }


}
