﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public int XAmount, YAmount;
    public GridSpace gSpace;
    public List<List<GridSpace>> grid = new List<List<GridSpace>>();

    public GameObject wallPrefab;
    public MapGenerator mg;

    public GridSpace LastMOver = null;
  

    public void SpawnGrid()
    {
        int x = 0, y = 0;

        for (int i = 0; i < XAmount; i++)
        {
            x += 1;
            List<GridSpace> g = new List<GridSpace>();
            for (int j = 0; j < YAmount; j++)
            {
                GridSpace gs = GameObject.Instantiate(gSpace, new Vector3(x, 0, y), Quaternion.identity) as GridSpace;
                gs.xPosition = x - 1;
                gs.yPosition = y;

                g.Add(gs);
                y += 1;
            }
            grid.Add(g);
            y = 0;
        }
    }

    public void SpawnGridTest()
    {
        mg.GenerateMap();
    }

    public void test()
    {
        for (int i = 0; i < XAmount; i++)
        {
            for (int j = 0; j < YAmount; j++)
            {
                grid[i][j].GetComponent<Renderer>().material.color = Color.green;
            }
        }
    }

    public GridSpace getSpace(int x, int y)
    {
        try
        {
            return grid[x][y];
        }
        catch
        {
            return null;
        }
       
    }

    public float getDistanceBetweenEntities(Entity e1, Entity e2)
    {
        return Vector2.Distance(e1.GetEntityPosition(), e2.GetEntityPosition());
    }
    public float getDistanceBetweenGridSpaces(GridSpace gs1, GridSpace gs2)
    {
        return Vector2.Distance(gs1.getPosition(), gs2.getPosition());
    }
}
