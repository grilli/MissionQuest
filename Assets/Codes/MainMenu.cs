﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenuObj;
    public GameObject levelSelectObj;
    public GameObject HowToPlayPopUp;
    public GameObject aFileSelector;
    //public MapGenerator MapGenerator;
    public string CustomMap;
    public InputField filePathText;

    [DllImport("user32.dll")]
    private static extern void SaveFileDialog(); //in your case : OpenFileDialog


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

public void BackToMainMenu()
    {
        SoundManager.PlayASource("Button");
        levelSelectObj.SetActive(false);
        mainMenuObj.SetActive(true);    
    }

    public void ToLevelSelect()
    {
        SoundManager.PlayASource("Button");
        mainMenuObj.SetActive(false);
        levelSelectObj.SetActive(true);
        
    }

    public void QuitGame()
    {
        SoundManager.PlayASource("Button");
        Application.Quit();
    }

    public void SelectLevel1()
    {
        SoundManager.PlayASource("Button");
        MapGenerator.levelToGenerate = "Map1.txt";
        Application.LoadLevel("Scene");
        
        //MapGenerator.GenerateMap
    }
    public void SelectLevel2()
    {
        SoundManager.PlayASource("Button");
        MapGenerator.levelToGenerate = "Map2.txt";
        Application.LoadLevel("Scene");
    }
    public void OpenFileDialog()
    {
#if UNITY_ANDROID
        SoundManager.PlayASource("Button");
        if (aFileSelector.activeSelf)
            aFileSelector.SetActive(false);
        else
            aFileSelector.SetActive(true);
#else
        SoundManager.PlayASource("Button");
        //System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
        System.Windows.Forms.OpenFileDialog joku = new System.Windows.Forms.OpenFileDialog();
        joku.ShowDialog();
        CustomMap = joku.FileName;
        filePathText.text = CustomMap;
#endif
    }
    public void LoadCustomMap()
    {
        SoundManager.PlayASource("Button");
#if UNITY_ANDROID
        string s = filePathText.text;
#else
        string s = System.IO.Path.GetFileName(CustomMap);
#endif
        MapGenerator.levelToGenerate = s;
        Application.LoadLevel("Scene");
    }
    public void ShowhowToPlay()
    {
        SoundManager.PlayASource("Button");
        HowToPlayPopUp.SetActive(true);
    }
}