﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(GridManager))]
public class AStarPathfinding : MonoBehaviour
{
    GridManager gm;
    List<List<GridSpace>> map;

    GridSpace goalNode;
    GridSpace startNode;

    GridSpace curNode;

    List<GridSpace> path = new List<GridSpace>();

    [HideInInspector]
    public int enemyCharIndex;

    private void Awake()
    {
        gm = GetComponent<GridManager>();
        map = gm.grid;
    }

    ///Call this method if you want to move something from start point to destination.
    public void Pathfinding(Vector2 from, Vector2 destination, int moves, bool move)
    {
        if (from == destination)
            return;

        goalNode = gm.getSpace((int)destination.x, (int)destination.y);

        startNode = gm.getSpace((int)from.x, (int)from.y); //new GridSpace(); // from, 0, Heuristics.ManhattanDistance(from, destination));
        startNode.f_score = Heuristics.ManhattanDistance(from, destination);
        startNode.canBeWalked = true;

        List<GridSpace> open = new List<GridSpace>(); //list of nodes
        List<GridSpace> closed = new List<GridSpace>();
        open.Add(startNode); //Add starting point

        while (open.Count > 0)
        {
            //Get node with lowest F value
             curNode = getBestNode(open); 

            if (curNode.getPosition() == goalNode.getPosition())
            {
                //reconstruct path here
                GridSpace curGs = goalNode;
                path.Add(curGs);
                while(curGs.parent != startNode)
                {
                    curGs = curGs.parent;
                    path.Add(curGs);
                }

                path.Reverse();
                break;
            }

            open.Remove(curNode);
            closed.Add(curNode);

            List<GridSpace> neighbors = FindValidFourNeighbours(curNode);

            //check neighbors for best f_score
            foreach (GridSpace n in neighbors)
            {
                float g_score = curNode.G + 1;
                float h_score = Heuristics.ManhattanDistance(n.getPosition(), goalNode.getPosition());
                float f_score = g_score + h_score;

                //skip higher scored
                if (isValueInList(n, closed) && f_score >= n.f_score)
                    continue;

                //skip walls
                if (!curNode.canBeWalked || curNode.isWall)
                    continue;

                //if score < then add to open, if its not there already
                if (!isValueInList(n, open) || f_score < n.f_score)
                {
                    //set parent to best f_score neighbor
                    n.parent = curNode;
                    n.G = g_score;
                    n.H = h_score;

                    if (!isValueInList(n, open))
                    {
                        open.Add(n);
                    }
                }
            }
        }

        startNode.canBeWalked = false;
        MovePath(path, moves, move);

    }

    ///Returns path from to destination
    public List<GridSpace> GetPath(Vector2 from, Vector2 destination, bool skipEntities)
    {
        List<GridSpace> pathToReturn = new List<GridSpace>();

        if (from == destination)
            return pathToReturn;

        goalNode = gm.getSpace((int)destination.x, (int)destination.y);

        startNode = gm.getSpace((int)from.x, (int)from.y); //new GridSpace(); // from, 0, Heuristics.ManhattanDistance(from, destination));
        startNode.f_score = Heuristics.ManhattanDistance(from, destination);

        //this is done so pathfinding doesnt stop at our tile. Needs to be reset afther.
        startNode.canBeWalked = true;

        List<GridSpace> open = new List<GridSpace>(); //list of nodes
        List<GridSpace> closed = new List<GridSpace>();
        open.Add(startNode); //Add starting point

        while (open.Count > 0)
        {
            //Get node with lowest F value
            curNode = getBestNode(open);

            if (curNode.getPosition() == goalNode.getPosition())
            {
                //reconstruct path here
                GridSpace curGs = goalNode;
                pathToReturn.Add(curGs);
                while (curGs.parent != startNode)
                {
                    curGs = curGs.parent;
                    pathToReturn.Add(curGs);
                }

                pathToReturn.Reverse();
                break;
            }

            open.Remove(curNode);
            closed.Add(curNode);

            List<GridSpace> neighbors = FindValidFourNeighbours(curNode);

            //check neighbors for best f_score
            foreach (GridSpace n in neighbors)
            {
                float g_score = curNode.G + 1;
                float h_score = Heuristics.ManhattanDistance(n.getPosition(), goalNode.getPosition());
                float f_score = g_score + h_score;

                //skip higher scored
                if (isValueInList(n, closed) && f_score >= n.f_score)
                    continue;

                //skip walls
                if (!curNode.canBeWalked && !curNode.hasEntity || curNode.isWall)
                    continue;

                //skipEntities
                if (!skipEntities && !curNode.canBeWalked)
                    continue;

                //skip player entities
                if (curNode.hasEntity && curNode.charOnTile.eOwner == EntityOwner.Player)
                    continue;

                //skip closed doors
                if (curNode.isDoor && curNode.doorObj != null && curNode.doorObj.activeSelf)
                    continue;

                //skip hidden
                if (curNode.isHidden)
                    continue;

                //if score < then add to open, if its not there already
                if (!isValueInList(n, open) || f_score < n.f_score)
                {
                    //set parent to best f_score neighbor
                    n.parent = curNode;
                    n.G = g_score;
                    n.H = h_score;

                    if (!isValueInList(n, open))
                    {
                        open.Add(n);
                    }
                }
            }
        }

        startNode.canBeWalked = false;
        return pathToReturn;
    }

    ///Called by pathfinding method when it has finished. Starts "WaitMove" coroutine if the path was found and changes the color of path to red so we can see it
    public void MovePath(List<GridSpace> path, int moves, bool move)
    {
        bool foundPath = (path.Count > 0) ? true : false;

        GameManager.GM.ResetNodes();
        for (int i = 0; i < path.Count; i++)
        {
            if (i < moves)
                path[i].GetComponent<Renderer>().material.color = Color.green;
            else
                path[i].GetComponent<Renderer>().material.color = Color.red;
        }
        
        if(move)
            StartCoroutine(WaitMove(path, foundPath, moves));
        else
            resetPath();
    }

    ///Called by "MovePath" method handles the actual moving and waiting between moved spaces
    IEnumerator WaitMove(List<GridSpace> gl, bool foundPath, int moves)
    {
        if(GameManager.GM.currentTurnType == Turn.Player)
            GameManager.GM.canAct = false;

        if (foundPath)
            ScreenLog.PrintToLog("Moving to " + gl.Last<GridSpace>().getPosition().ToString());
        /*{
        gm.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).canBeWalked = true;
        }
        else
            gm.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).canBeWalked = false;*/

        //check if we have more moves than the desired location
        if (moves > gl.Count)
        {
            for (int i = 0; i < ((GameManager.GM.currentTurnType == Turn.Player) ? gl.Count : gl.Count - 1); i++)
            {
                if(!gl[i].canBeWalked)
                {
                    break;
                }

                movementRotation(gl ,i);
                GameManager.GM.gridManager.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).charOnTile = null;
                GameManager.GM.gridManager.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).hasEntity = false;
                GameManager.GM.currentMovingEntity.SetEntityPosition(gl[i].xPosition, gl[i].yPosition);
                if (GameManager.GM.currentMovingEntity.eOwner == EntityOwner.AI)
                    GameManager.GM.FocusToCurrentEntity();
                yield return new WaitForSeconds(0.25f);
            }
        }
        //handles when we have same amount or less moves than desired location
        else
        {
            for (int i = 0; i < moves; i++)
            {
                if(GameManager.GM.currentTurnType == Turn.Enemy)
                { // AI movement
                    //check if next position is playerPos
                    if(FindObjectOfType<AIController>().currentTarget.GetEntityPosition() == gl[i].getPosition() || !gl[i].canBeWalked)
                    {
                        break;
                    }
                    else
                    {
                        movementRotation(gl, i);
                        GameManager.GM.gridManager.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).charOnTile = null;
                        GameManager.GM.gridManager.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).hasEntity = false;
                        GameManager.GM.currentMovingEntity.SetEntityPosition(gl[i].xPosition, gl[i].yPosition);
                        if(GameManager.GM.currentMovingEntity.eOwner == EntityOwner.AI)
                            GameManager.GM.FocusToCurrentEntity();
                        yield return new WaitForSeconds(0.25f);
                    }
                }
                else
                { // Player movement
                    movementRotation(gl, i);
                    GameManager.GM.gridManager.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).charOnTile = null;
                    GameManager.GM.gridManager.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).hasEntity = false;
                    GameManager.GM.currentMovingEntity.SetEntityPosition(gl[i].xPosition, gl[i].yPosition);
                    if (GameManager.GM.currentMovingEntity.eOwner == EntityOwner.AI)
                        GameManager.GM.FocusToCurrentEntity();
                    yield return new WaitForSeconds(0.25f);
                }
            }
        }

        //make so our current spot cant be walked to
        gm.getSpace(GameManager.GM.currentMovingEntity.xPosInGrid, GameManager.GM.currentMovingEntity.yPosInGrid).canBeWalked = false;

        //reset whole shit afther we moved
        resetPath();

        path.Clear();

        if (GameManager.GM.currentTurnType == Turn.Player)
            GameManager.GM.canAct = true;

        if(foundPath && GameManager.GM.currentTurnType == Turn.Player)
        {
            FindObjectOfType<PlayerController>().hasMoved = true;
            GameManager.GM.playerCanMove = false;
            GameManager.GM.ResetNodes();
        }
        else if(GameManager.GM.currentTurnType == Turn.Enemy)
        {
            GameManager.GM.ResetNodes();
        }
           
    }

    void resetPath()
    {
        foreach (List<GridSpace> lg in map)
        {
            foreach (GridSpace g in lg)
            {
                g.parent = null;
                g.f_score = 0;
                g.G = 0;
                g.H = 0;
            }
        }

        path.Clear();
    }

    bool isValueInList(GridSpace node, List<GridSpace> map)
    {
        bool rVal = false;

        for (int i = 0; i < map.Count; i++)
            if (map[i] == node)
                rVal = true;
        return rVal;
    }

    //Gets best f_score node out of nodelist
    GridSpace getBestNode(List<GridSpace> nodes)
    {
        float maxF = nodes.Max(obj => obj.f_score);
        return (GridSpace)nodes.First(obj => obj.f_score == maxF);
    }

    //Find neighbours for the given node (up,down,left,right)
    public List<GridSpace> FindValidFourNeighbours(GridSpace node)
    {
        List<GridSpace> neighbours = new List<GridSpace>();
        GridSpace vn;

        //one node down
        vn = gm.getSpace(node.xPosition, node.yPosition - 1);
        if(vn != null)
            neighbours.Add(vn);

        //one node up
        vn = gm.getSpace(node.xPosition, node.yPosition + 1);
        if (vn != null)
            neighbours.Add(vn);

        //one node to left
        vn = gm.getSpace(node.xPosition - 1, node.yPosition);
        if (vn != null)
            neighbours.Add(vn);

        //one node to right
        vn = gm.getSpace(node.xPosition + 1, node.yPosition);
        if (vn != null)
            neighbours.Add(vn);

        /*foreach (GridSpace g in neighbours)
            g.GetComponent<Renderer>().material.color = Color.blue;*/

        return neighbours;
    }

    // Rotation for movement and combat
    public void movementRotation(List<GridSpace> gl, int glPosition)
    {
        Vector3 faceUp = new Vector3(0, 0, 0);
        Vector3 faceRight = new Vector3(0, 90, 0);
        Vector3 faceDown = new Vector3(0, 180, 0);
        Vector3 faceLeft = new Vector3(0, 270, 0);
        Vector3 compare = new Vector3((int)GameManager.GM.currentMovingEntity.transform.rotation.eulerAngles.x, (int)GameManager.GM.currentMovingEntity.transform.rotation.eulerAngles.y, (int)GameManager.GM.currentMovingEntity.transform.rotation.eulerAngles.z);
        int gap = 10;

        if (GameManager.GM.currentMovingEntity.xPosInGrid + 1 == gl[glPosition].xPosition && GameManager.GM.currentMovingEntity.yPosInGrid == gl[glPosition].yPosition) // Going up
        {
            if (compare.y < faceRight.y + gap && compare.y > faceRight.y - gap) // Facing right
                GameManager.GM.currentMovingEntity.transform.Rotate(0, -90, 0);

            else if (compare.y < faceDown.y + gap && compare.y > faceDown.y - gap) // Facing down
                GameManager.GM.currentMovingEntity.transform.Rotate(0, -180, 0);

            else if (compare.y < faceLeft.y + gap && compare.y > faceLeft.y - gap) // Facing left
                GameManager.GM.currentMovingEntity.transform.Rotate(0, 90, 0);
        }
        else if (GameManager.GM.currentMovingEntity.xPosInGrid - 1 == gl[glPosition].xPosition && GameManager.GM.currentMovingEntity.yPosInGrid == gl[glPosition].yPosition) // Going down
        {
            if (compare.y < faceUp.y + gap && compare.y > faceUp.y - gap) // Facing up
                GameManager.GM.currentMovingEntity.transform.Rotate(0, 180, 0);
            
            else if (compare.y < faceRight.y + gap && compare.y > faceRight.y - gap) // Facing right
                GameManager.GM.currentMovingEntity.transform.Rotate(0, 90, 0);
            
            else if (compare.y < faceLeft.y + gap && compare.y > faceLeft.y - gap) // Facing left
                GameManager.GM.currentMovingEntity.transform.Rotate(0, -90, 0);
        }
        else if (GameManager.GM.currentMovingEntity.xPosInGrid == gl[glPosition].xPosition && GameManager.GM.currentMovingEntity.yPosInGrid + 1 == gl[glPosition].yPosition) // Going right
        {
            if (compare.y < faceUp.y + gap && compare.y > faceUp.y - gap) // Facing up
                GameManager.GM.currentMovingEntity.transform.Rotate(0, 90, 0);

            else if (compare.y < faceDown.y + gap && compare.y > faceDown.y - gap) // Facing down
                GameManager.GM.currentMovingEntity.transform.Rotate(0, -90, 0);
            
            else if (compare.y < faceLeft.y + gap && compare.y > faceLeft.y - gap) // Facing left
                GameManager.GM.currentMovingEntity.transform.Rotate(0, 180, 0);
        }
        else if (GameManager.GM.currentMovingEntity.xPosInGrid == gl[glPosition].xPosition && GameManager.GM.currentMovingEntity.yPosInGrid - 1 == gl[glPosition].yPosition) // Going left
        {
            if (compare.y < faceUp.y + gap && compare.y > faceUp.y - gap) // Facing up
                GameManager.GM.currentMovingEntity.transform.Rotate(0, -90, 0);
            
            else if (compare.y < faceRight.y + gap && compare.y > faceRight.y - gap) // Facing right
                GameManager.GM.currentMovingEntity.transform.Rotate(0, 180, 0);
            
            else if (compare.y < faceDown.y + gap && compare.y > faceDown.y - gap) // Facing down
                GameManager.GM.currentMovingEntity.transform.Rotate(0, 90, 0);
        }
    }
}
