﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public GameObject PauseMenuObj;
    bool canDoAction;

    // Use this for initialization
    void Start ()
    {
        Time.timeScale = 1;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(Time.timeScale == 1)
            {
                Time.timeScale = 0;
                PauseMenuObj.SetActive(true);
                canDoAction = GameManager.GM.canAct;
                GameManager.GM.canAct = false;
            }
            else
            {
                Time.timeScale = 1;
                PauseMenuObj.SetActive(false);
                GameManager.GM.canAct = canDoAction;
            }
        }
	}
    public void ResumeGame()
    {
        Time.timeScale = 1;
        PauseMenuObj.SetActive(false);
        
    }
}
