﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Dice
{


    public static int RollMovementDice(int dAmount)
    {
        
        int sum=0;

        for (int i=0; i<dAmount; i++)
        {
            sum += Random.Range(1, 7);
        }

        ScreenLog.PrintToLog(GameManager.GM.currentTurnType.ToString() + " rolled " + sum + " for movement");

        return sum;
    }

    public static int RollCombatDice()
    {

        int dValue =  Random.Range(1, 11);
        
        //ScreenLog.PrintToLog(GameManager.GM.currentTurnType.ToString() + " rolled " + sum);

        return dValue;
    }
}
