﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager GM;
    public GridManager gridManager;
    public Text turnUi;
    public Text whosTurn;
    public Text currentGridPosText;
    public MapGenerator mapGenerator;
    public GameObject victoryScreen;
    public GameObject defeatScreen;
    public GameObject quitWindow;

    public Canvas worldNumber;

    public GameObject fireParticle;

    public int CurrentTurn = 0;
    public Turn currentTurnType;

    [SerializeField]
    private Entity playerPrefab;

    public Entity currentMovingEntity;

    public int playerRoll;

    public bool playerCanMove;
    public bool playerCanAttack;

    public bool playerMeleeAttack;
    public bool playerRangedAttack;
    public bool playerMagicAttack;

    public bool chestCanBeOpened = false;

    public bool canAct;

    public bool hasFinalKey = false;

    public InspectScreen isc;

    private void Awake()
    {
#if UNITY_ANDROID
        StartCoroutine(AStartGame());
#else
        GM = this;
        isc = FindObjectOfType<InspectScreen>();
        isc.gameObject.SetActive(false);

        gridManager = FindObjectOfType<GridManager>();
        gridManager.SpawnGridTest();
        canAct = true;
        PlayerController.score = 0;
        ScreenLog.PrintToLog("Game started");

        NextTurn();
#endif
    }

    private IEnumerator AStartGame()
    {
        GM = this;
        isc = FindObjectOfType<InspectScreen>();
        isc.gameObject.SetActive(false);
        gridManager = FindObjectOfType<GridManager>();
        mapGenerator.AGenerateMap();

        while (!mapGenerator.aMapLoadComplete)
            yield return null;

        canAct = true;
        PlayerController.score = 0;
        ScreenLog.PrintToLog("Game started");
        NextTurn();
    }

    private void Update()
    {
#if UNITY_ANDROID
        if (Input.touchCount > 0 && Input.GetTouch(0).tapCount == 2 && currentMovingEntity != null)
        {
            FocusToCurrentEntity();
        }
#else
        if (Input.GetKeyDown(KeyCode.F) && currentMovingEntity != null)
        {
            FocusToCurrentEntity();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if(quitWindow.activeSelf)
                quitWindow.SetActive(false);
            else
                quitWindow.SetActive(true);
        }
#endif
    }

    ///Resets all the nodes
    public void ResetNodes()
    {
        for (int i = 0; i < gridManager.XAmount; i++)
        {
            for (int j = 0; j < gridManager.YAmount; j++)
            {
                gridManager.grid[i][j].DeSelect();
            }
        }
    }

    ///Called to end turn and start new one. Handles logic for turn swaps and calls Either AI or Player controllers "DoTurn" method
    public void NextTurn()
    {
        //reset all nodes
        ResetNodes();

        if (CurrentTurn == 0)
        {
            CurrentTurn++;
            turnUi.text = "Turn: " + CurrentTurn.ToString();
            currentTurnType = Turn.Player;
            whosTurn.text = currentTurnType.ToString();
        }
        else
        {
            CurrentTurn++;
            turnUi.text = "Turn: " + CurrentTurn.ToString();
            currentTurnType = (currentTurnType == Turn.Player) ? Turn.Enemy : Turn.Player;
            whosTurn.text = currentTurnType.ToString();
        }

        if (currentTurnType == Turn.Enemy)
        {
            SoundManager.PlayASource("EnemyTurn");
            //We can use colors with <color=#00000></color> html tags.. AMAZING!
            ScreenLog.PrintToLog("<color=#ff0000ff>CURRENT TURN " + currentTurnType.ToString().ToUpper() + "</color>");
            FindObjectOfType<AIController>().DoTurn();
        }
        else
        {
            SoundManager.PlayASource("PlayerTurn");
            ScreenLog.PrintToLog("<color=#00ff00ff>CURRENT TURN " + currentTurnType.ToString().ToUpper() + "</color>");
            FindObjectOfType<PlayerController>().DoTurn();
            //currentMovingEntity.action.PerformAction(ActionType.Move);
        }
    }

    ///Spawns entity to x,y position on grid
    public Entity SpawnEntity(Entity prefab, int x, int y)
    {
        Entity p = GameObject.Instantiate(prefab, gridManager.getSpace(x, y).transform.position + Vector3.up, Quaternion.Euler(0,180,0)) as Entity;
        p.xPosInGrid = x;
        p.yPosInGrid = y;

        gridManager.getSpace(x, y).charOnTile = p;
        gridManager.getSpace(x, y).canBeWalked = false;
        gridManager.getSpace(x, y).hasEntity = true;

        return p;
    }
    public void LoadLevel(string levelName)
    {
        Application.LoadLevel(levelName);
    }
    public void FocusToCurrentEntity()
    {
        Camera.main.transform.position = new Vector3(currentMovingEntity.transform.position.x, Camera.main.transform.position.y, currentMovingEntity.transform.position.z);
    }
}

public enum Turn
{
    Player,
    Enemy
};


