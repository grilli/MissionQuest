﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    public List<Entity> enemyPrefab = new List<Entity>();

    public List<Entity> enemies = new List<Entity>();

    public GridManager gm;

    [HideInInspector]
    public Entity currentTarget = null;

    //private Entity player;

    public int currentCharacterIndex;

    private void Awake()
    {
        gm = FindObjectOfType<GridManager>();
    }

    private void Start()
    {
        //player = GameManager.GM.playerEntity;
        //InitializeEnemies();
    }

    /*public void InitializeEnemies()
    {
        for(int i = 0; i < enemyAmountToSpawn; i++)
        {
            Entity e = GameManager.GM.SpawnEntity(enemyPrefab, (int)spawnSpots[i].x, (int)spawnSpots[i].y);
            e.eOwner = EntityOwner.AI;
            enemies.Add(e);
        }
    }*/

    ///Call this to start AI turn logic
    public void DoTurn()
    {
        StartCoroutine(DoTurnLogic());
    }

    ///Handles logic for the turn including waitings etc.
    IEnumerator DoTurnLogic()
    {
        GameManager.GM.canAct = false;
        for (int i = 0; i < enemies.Count; i++)
        {
            //check if enemy is enabled before trying to do something with
            if(!enemies[i].isDisabled)
            {
                yield return StartCoroutine(DoTurnForCharacter(i));
            }
        }

        yield return new WaitForSeconds(1);
        GameManager.GM.canAct = true;
        GameManager.GM.NextTurn();
    }

    ///Does turn logic for specific character in AI array. Called by "DoTurnLogic" IEnumerator
    public IEnumerator DoTurnForCharacter(int charIndex)
    {
        int maxStepsToChace = 30;
        Entity curEnemy = enemies[charIndex];
        currentTarget = null;

        List<GridSpace> path = new List<GridSpace>();

        //float oldDist = 0;
        //float newDist = 0;

        bool usingRealPath = false;

        //Check the closest player entity to chase
        foreach (Entity e in FindObjectOfType<PlayerController>().playerEntities)
        {
            if (e.isDead) //skip dead
                continue;

            if (currentTarget == null)
            {
                List<GridSpace> tp = FindObjectOfType<AStarPathfinding>().GetPath(curEnemy.GetEntityPosition(), e.GetEntityPosition(), false);
                if(tp.Count < maxStepsToChace)
                {
                    currentTarget = e;
                    path = tp;
                    //FindObjectOfType<AStarPathfinding>().GetPath(curEnemy.GetEntityPosition(), e.GetEntityPosition(), false);
                }
                //oldDist = GameManager.GM.gridManager.getDistanceBetweenEntities(curEnemy, e);
            }

            List<GridSpace> tempPath = FindObjectOfType<AStarPathfinding>().GetPath(curEnemy.GetEntityPosition(), e.GetEntityPosition(), false);
            //newDist = GameManager.GM.gridManager.getDistanceBetweenEntities(curEnemy, e);

            //if we are next to target we want to hit, we exit
            if(tempPath.Count == 1)
            {
                currentTarget = e;
                path = tempPath;
                //oldDist = newDist;
                break;
            }

            //there is a path
            else if(tempPath.Count > 0 && tempPath.Count < maxStepsToChace)
            {
                if(usingRealPath)
                {
                    //there already is valid path and new path is shorter
                    if (path.Count > 0 && tempPath.Count < path.Count)
                    {
                        currentTarget = e;
                        path = tempPath;
                        //oldDist = newDist;
                        usingRealPath = true;
                    }
                }
                else //we dont have real path yet so this is best path sofar
                {
                    currentTarget = e;
                    path = tempPath;
                    //oldDist = newDist;
                    usingRealPath = true;
                }
            }

            //there is no path or its too long and we dont have real path already
            else if(tempPath.Count == 0 && !usingRealPath /*|| tempPath.Count > 15 && !usingRealPath*/)
            {
                //if we dont have path, we might aswell move as close as we can
                tempPath = FindObjectOfType<AStarPathfinding>().GetPath(curEnemy.GetEntityPosition(), e.GetEntityPosition(), true);
                if(tempPath.Count > 0 && tempPath.Count < maxStepsToChace)
                {
                    //we have fake path already,
                    if(path.Count > 0 && tempPath.Count < path.Count)
                    {
                        currentTarget = e;
                        path = tempPath;
                        //oldDist = newDist;
                    }
                    else //dont have a path already so using this
                    {
                        currentTarget = e;
                        path = tempPath;
                        //oldDist = newDist;
                    }
                } 
            }
        }

        GameManager.GM.currentMovingEntity = curEnemy;
        GameManager.GM.FocusToCurrentEntity();

        if (currentTarget != null)
        {
            //we are close enough to hit
            if (gm.getDistanceBetweenEntities(curEnemy, currentTarget) < 1.2f)
            {
                ScreenLog.PrintToLog("Enemy: " + "<color=#ff0000ff>" + curEnemy.eName + "</color> attacked:");
                curEnemy.action.PerformActionAI(ActionType.Attack, charIndex, null);
                yield return new WaitForSeconds(1);
            }
            //move closer
            else
            {
                ScreenLog.PrintToLog("Enemy: " + "<color=#ff0000ff>" + curEnemy.eName + "</color> moving");
                curEnemy.action.PerformActionAI(ActionType.Move, charIndex, path);

                if (path.Count > 0)
                    yield return new WaitForSeconds(1.5f);
                else
                    yield return new WaitForSeconds(0.5f);

                //are we close enough now?
                if (gm.getDistanceBetweenEntities(curEnemy, currentTarget) < 1.2f)
                {
                    ScreenLog.PrintToLog("Enemy: " + "<color=#ff0000ff>" + curEnemy.eName + "</color> attacked");
                    curEnemy.action.PerformActionAI(ActionType.Attack, charIndex, null);
                    yield return new WaitForSeconds(1);
                }
            }
        }
    }
}
