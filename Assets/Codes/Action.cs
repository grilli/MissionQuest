﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[RequireComponent(typeof(Entity))]
public class Action : MonoBehaviour
{
    public int actionsLeft;
    public int maxActions;

    private Entity e;

    private void Awake()
    {
        actionsLeft = maxActions;
        e = GetComponent<Entity>();
    }

    public void InitTurn()
    {
        actionsLeft = maxActions;
    }

    //actions for player controlled entity
    public void PerformActionPlayer(ActionType a)
    {
       switch (a)
        {
            case ActionType.Move:
                Move(e.xPosInGrid, e.yPosInGrid);
                break;
            case ActionType.Attack:
                PerformAttack(e, e.attackType);
                break;
            case ActionType.Search: // Not used for anything ATM
                Search();
                break;
            case ActionType.OpenDoor:
                OpenDoor();
                break;
            case ActionType.OpenChest:
                //OpenChest();
                break;
        }
    }
    
    //actions for ai controlled entity
    public void PerformActionAI(ActionType a, int charIndex, List<GridSpace> path)
    {
        switch (a)
        {
            case ActionType.Move:
                EnemyMove(e.xPosInGrid, e.yPosInGrid, charIndex, path);
                break;
            case ActionType.Attack:
                List<GridSpace> gsl = new List<GridSpace>();
                gsl.Add(FindObjectOfType<GridManager>().getSpace(FindObjectOfType<AIController>().currentTarget.xPosInGrid, FindObjectOfType<AIController>().currentTarget.yPosInGrid));
                GameManager.GM.gridManager.GetComponent<AStarPathfinding>().movementRotation(gsl, 0);
                Attack(e, FindObjectOfType<AIController>().currentTarget, true);
                if(e.eName == "Zombie")
                    SoundManager.PlayASource("ZombieHit");
                else
                    SoundManager.PlayASource("SwordHit");
                break;
        }
    }

    public void PerformAttack(Entity e, AttackType at)
    {
        switch (at)
        {
            case AttackType.Melee:
                MeleeAttack(e);
                break;
            case AttackType.Ranged:
                RangedAttack(e);
                break;
            case AttackType.Magic:
                MagicAttack(e);
                break;
            case AttackType.BackStab:
                MeleeAttack(e);
                break;
        }
    }
    // // Old action system, just in case
    //public void PerformActionPlayer(ActionType a)
    //{
    //    switch (a)
    //    {
    //        case ActionType.Move:
    //            Move(e.xPosInGrid, e.yPosInGrid);
    //            break;
    //        case ActionType.MeleeAttack:
    //            MeleeAttack(e);
    //            break;
    //        case ActionType.RangedAttack:
    //            RangedAttack(e);
    //            break;
    //        case ActionType.MagicAttack:
    //            MagicAttack(e);
    //            break;
    //            //case ActionType.Search: // Not used for anything ATM
    //            //    Search();
    //            //    break;
    //            //case ActionType.OpenDoor:
    //            //    OpenDoor();
    //            //    break;
    //            //case ActionType.OpenChest:
    //            //    //OpenChest();
    //            //    break;
    //    }
    //}

    ////actions for ai controlled entity
    //public void PerformActionAI(ActionType a, int charIndex)
    //{
    //    switch (a)
    //    {
    //        case ActionType.Move:
    //            EnemyMove(e.xPosInGrid, e.yPosInGrid, charIndex);
    //            break;
    //        case ActionType.MeleeAttack:
    //            Attack(e, FindObjectOfType<AIController>().currentTarget, true);
    //            break;
    //        case ActionType.RangedAttack:
    //            Attack(e, FindObjectOfType<AIController>().currentTarget, false);
    //            break;
    //    }
    //}

    ///Does movement for the current entity, if its enemy
    public void EnemyMove(int xPos, int yPos, int charIndex, List<GridSpace> aiPath)
    {
        int dieRoll = Dice.RollMovementDice(e.stats.movementDiceAmount);
        //find path to "currentTarget" which is the closest target to our entity, logic for that is in "AIController"
        GameManager.GM.gridManager.GetComponent<AStarPathfinding>().MovePath(aiPath, dieRoll, true);
        //.Pathfinding(e.GetEntityPosition(), FindObjectOfType<AIController>().currentTarget.GetEntityPosition(), dieRoll, true);
    }

    public void Move(int xPos, int yPos)
    {
        GameManager.GM.playerCanMove = true;

        //This used to be the way to show player all the spots where he can move.
        //It doesn't work with walls so we dont use it anymore.
        //It however might be usefull in future, so it's in comments below.
        //Enjoy :)

        /*int dieRoll = Dice.RollMovementDice(2);
        GameManager.GM.playerRoll = dieRoll;

        GridManager gridManager = GameManager.GM.gridManager;

        //super inefficient as it loops through whole grid. If grid is massive this will be super slow!!
        for(int i = 0; i < gridManager.XAmount; i++)
        {
            for(int j = 0; j < gridManager.YAmount; j++)
            {
                if ((Mathf.Abs(xPos - i)) + (Mathf.Abs(yPos - j)) <= dieRoll)
                {
                    GridSpace gs = gridManager.getSpace(i, j);
                    gs.Select();

                    if (!gs.isWall)
                    {
                        gs.canBeWalked = true;
                    }
                }
                else
                {
                    GridSpace gs = gridManager.getSpace(i, j);
                    gs.DeSelect();

                    if (!gs.isWall)
                    {
                        gs.canBeWalked = false;
                    }
                }
            }
        }*/
    }

    public void Attack(Entity Attacker, Entity Defender, bool allowCritDef)
    {
        List<int> attacks = new List<int>();
        List<int> defences = new List<int>();
        int damageToDefender = 0;
        int damageToAttacker = 0;

        string aColor = "";
        string dColor = "";

        if (Attacker.eOwner == EntityOwner.Player)
            aColor = "<color=#00ff00ff>";
        else
            aColor = "<color=#ff0000ff>";

        if (Defender.eOwner == EntityOwner.Player)
            dColor = "<color=#00ff00ff>";
        else
            dColor = "<color=#ff0000ff>";


        // Rolls attack die
        for (int i = 0; i < Attacker.stats.attackDiceAmount; i++)
        {
            attacks.Add(Dice.RollCombatDice());
        }
        ScreenLog.PrintToLog(aColor + Attacker.eName + "</color> rolled " + attacks.Count + " dice for attack.");

        if (Attacker.attackType.ToString() == "BackStab" && (Attacker.transform.eulerAngles == Defender.transform.eulerAngles))
        {
            attacks.Add(Dice.RollCombatDice());
            ScreenLog.PrintToLog(aColor + Attacker.eName + "</color> surprises " + dColor + Defender.eName + "</color> with a <color=#ff0000ff>Backstab</color> and rolls one extra attack die.");
            allowCritDef = false; // Makes sure that backstab doesn't allow critdef
        }

        // Rolls defence die and checks for stoneskin potion
        for (int i = 0; i < ((Defender.stoneskinPotionUptime > 0) ? Defender.stats.defenceDiceAmount + 10 : Defender.stats.defenceDiceAmount); i++)
        {
            defences.Add(Dice.RollCombatDice());
        }

        // Calculates attack amount
        for (int i = 0; i < attacks.Count; i++)
        {
            if (attacks[i] == 10) // Critical hit
            {
                damageToDefender += 2;
                ScreenLog.PrintToLog(aColor + Attacker.eName + "</color> rolled " + attacks[i] + " causing a <color=#ff0000ff>critical hit</color>");
            }
            else if (attacks[i] <= 9 && attacks[i] >= 5) // Normal hit
            {
                damageToDefender++;
                ScreenLog.PrintToLog(aColor + Attacker.eName + "</color> rolled " + attacks[i] + " causing a <color=#ff0000ff>hit</color>");
            }
            else // Miss
                ScreenLog.PrintToLog(aColor + Attacker.eName + "</color> rolled " + attacks[i] + " causing a <color=#80aaff>miss</color>");
        }
        if (damageToDefender != 0) // Checks if damage is dealt
        {
            // Calculates amount of damage reduction from defence
            for (int i = 0; i < defences.Count; i++)
            {
                if (defences[i] == 10 && allowCritDef)  // Counter attack
                {
                    damageToDefender--;
                    damageToAttacker++; // Crit def causes a counter attack
                    ScreenLog.PrintToLog(dColor + Defender.eName + "</color> rolled " + defences[i] + " causing a <color=#ffffff>counter attack</color>");
                }
                else if (defences[i] >= 6) // Parry
                {
                    damageToDefender--;
                    ScreenLog.PrintToLog(dColor + Defender.eName + "</color> rolled " + defences[i] + " <color=#ff9966>blocking</color> a hit");
                }
            }
        }
        // If defence is higher than incoming damage, damage is reset so they don't heal
        if (damageToDefender < 0)
            damageToDefender = 0;
        if (damageToAttacker < 0)
            damageToAttacker = 0;
        
        ScreenLog.PrintToLog(aColor + Attacker.eName + "</color> dealt " + damageToDefender + " damage to " + dColor + Defender.eName + "</color>");

        Canvas cv = Instantiate(GameManager.GM.worldNumber, Defender.transform.position + Vector3.up * 1.5f, Quaternion.Euler(90,0,0));
        cv.transform.GetChild(1).GetComponent<Text>().text = damageToDefender.ToString();
        cv.transform.GetChild(0).GetComponent<Image>().color = Color.red;

        if (damageToAttacker > 0)
        {
            ScreenLog.PrintToLog(dColor + Defender.eName + "</color> dealt " + damageToAttacker + " damage to " + aColor + Attacker.eName + "</color>");
            Canvas cv2 = Instantiate(GameManager.GM.worldNumber, Attacker.transform.position + Vector3.up * 1.5f, Quaternion.Euler(90, 0, 0));
            cv2.transform.GetChild(1).GetComponent<Text>().text = damageToAttacker.ToString();
            cv2.transform.GetChild(0).GetComponent<Image>().color = Color.red;
        }
        
        Attacker.stats.TakeDamage(damageToAttacker);
        Defender.stats.TakeDamage(damageToDefender);

        //clear nodes
        GameManager.GM.ResetNodes();

        ScreenLog.PrintToLog("");
    }

    public void MeleeAttack(Entity Attacker)
    {
        GridManager grm = FindObjectOfType<GridManager>();
        List<GridSpace> neighbouringEntities = new List<GridSpace>();
        GridSpace vn;

        //one node down
        vn = grm.getSpace(Attacker.xPosInGrid, Attacker.yPosInGrid - 1);
        if (vn.hasEntity)
            neighbouringEntities.Add(vn);
        //one node up
        vn = grm.getSpace(Attacker.xPosInGrid, Attacker.yPosInGrid + 1);
        if (vn.hasEntity)
            neighbouringEntities.Add(vn);
        //one node to left
        vn = grm.getSpace(Attacker.xPosInGrid - 1, Attacker.yPosInGrid);
        if (vn.hasEntity)
            neighbouringEntities.Add(vn);
        //one node to right
        vn = grm.getSpace(Attacker.xPosInGrid + 1, Attacker.yPosInGrid);
        if (vn.hasEntity)
            neighbouringEntities.Add(vn);

        foreach (GridSpace g in neighbouringEntities)
        {
            g.GetComponent<Renderer>().material.color = Color.blue;
            g.attackSelect = true;
        }
        
        GameManager.GM.playerCanAttack = true;
        GameManager.GM.playerMeleeAttack = true;
    }

    public void RangedAttack(Entity Attacker)
    {
        GridManager grm = FindObjectOfType<GridManager>();
        List<GridSpace> neighbouringEntities = new List<GridSpace>();
        AIController ac = FindObjectOfType<AIController>();
        
        for(int i = 0; i < ac.enemies.Count; i++)
        {
            if (!ac.enemies[i].isDisabled && !ac.enemies[i].isDead&&((int)GameManager.GM.gridManager.getDistanceBetweenEntities(Attacker, ac.enemies[i]) <= Attacker.stats.attackRange))
            {
                neighbouringEntities.Add(GameManager.GM.gridManager.getSpace(ac.enemies[i].xPosInGrid, ac.enemies[i].yPosInGrid));
            }
        }

        foreach (GridSpace g in neighbouringEntities)
        {
            g.GetComponent<Renderer>().material.color = Color.blue;
            g.attackSelect = true;
        }

        GameManager.GM.playerCanAttack = true;
        GameManager.GM.playerRangedAttack = true;
    }

    public void MagicAttack(Entity Attacker)
    {
        GameManager.GM.playerCanAttack = true;
        GameManager.GM.playerMagicAttack = true;
    }

    public void Search()
    {

    }

    public void OpenDoor()
    {
        
    }

    public void OpenChest(Entity entity, GridSpace gs)
    {
        if(GameManager.GM.chestCanBeOpened)
        {
            //gs.chest.chestLoot();
        }
    }
}

public enum ActionType
{
    Move,
    Attack,
    Search,
    OpenDoor,
    OpenChest,
};

/*public enum ActionType
{
    Move,
    MeleeAttack,
    RangedAttack,
    MagicAttack,
    Search,
    OpenDoor,
    OpenChest,
};*/