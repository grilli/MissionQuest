﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InspectScreen : MonoBehaviour
{
    public Text nameText;
    public Text stats;
    public Image image;

    public Image weapon;
    public Image armor;
    public Image shield;

    public Sprite noEquipped;


    public void UpdateInspectScreen(Entity e)
    {
        Stats s = e.GetComponent<Stats>();
        nameText.text = e.eName;
        string sString = "Level: " + s.level + "\n" +
                         "Health: " + s.health + " / " + s.maxHealth + "\n" +
                         "Attack Dice: " + s.attackDiceAmount + "\n" +
                         "Defence Dice: " + s.defenceDiceAmount + "\n" +
                         "Movement Dice: " + s.movementDiceAmount;
        stats.text = sString;

        if (e.stats.weapon.iName != "")
            weapon.sprite = e.stats.weapon.image;
        else
            weapon.sprite = noEquipped;

        if (e.stats.armor.iName != "")
            armor.sprite = e.stats.armor.image;
        else
            armor.sprite = noEquipped;

        if (e.stats.shield.iName != "")
            shield.sprite = e.stats.shield.image;
        else
            shield.sprite = noEquipped;

        image.sprite = e.image;
    }

    public void Close()
    {
        this.gameObject.SetActive(false);
        GameManager.GM.canAct = true;
    }
	
}
