﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public List<Entity> playerPrefabs = new List<Entity>();

    public List<Entity> playerEntities = new List<Entity>();

    public Entity currentActingEntity;

    public int moveRoll = 0;

    public bool hasMoved = false;
    public bool hasAttacked = false;

    int eIndex = 0;

    public Inventory inventory;

    public Text scoreText;

    public static int score = 0;

    private bool firstFocusDone = false;

    private void Awake()
    {
        inventory = GetComponent<Inventory>();
    }

    public void UpdateScoreText()
    {
        scoreText.text = "Score: " + score.ToString();
    }

    ///Spanws all player characters at the start
    /*public void Initialize()
    {
        for (int i = 0; i < playerPrefabs.Count; i++)
        {
            Entity e = GameManager.GM.SpawnEntity(playerPrefabs[i], (int)playerStartPositions[i].x, (int)playerStartPositions[i].y);
            e.eOwner = EntityOwner.Player;
            playerEntities.Add(e);
        }
    }*/

    ///Handles logic for players turn
    public void DoTurn()
    {
        if(CheckIfLose())
        {
            GameManager.GM.canAct = false;
            GameManager.GM.defeatScreen.SetActive(true);
            GameManager.GM.defeatScreen.transform.GetChild(1).GetComponent<Text>().text = "Your Score: " + PlayerController.score.ToString() + "!";
            SoundManager.PlayASource("Lose");
        }
        
        currentActingEntity = playerEntities[eIndex];
        GameManager.GM.currentMovingEntity = playerEntities[eIndex];
        ScreenLog.PrintToLog("<color=#00ff00ff>" + playerEntities[eIndex].eName + "s" + "</color>" + " turn");
        GameManager.GM.playerCanAttack = false;
        GameManager.GM.playerCanMove = false;
        GameManager.GM.playerMeleeAttack = false;
        GameManager.GM.playerRangedAttack = false;
        GameManager.GM.playerMagicAttack = false;
        hasMoved = false;
        hasAttacked = false;

        //If speedpotion buff is up our characters roll +2 movement dice
        if (currentActingEntity.speedPotionUptime > 0 && currentActingEntity.stoneskinPotionUptime == 0)
            moveRoll = Dice.RollMovementDice(currentActingEntity.stats.movementDiceAmount + 2);
        else if (currentActingEntity.stoneskinPotionUptime > 0)
            moveRoll = 0;
        else
            moveRoll = Dice.RollMovementDice(currentActingEntity.stats.movementDiceAmount);

        if (GameManager.GM.CurrentTurn == 1)
        {
            currentActingEntity.ClearHidden(GameManager.GM.gridManager.getSpace(currentActingEntity.xPosInGrid, currentActingEntity.yPosInGrid));
            //move camera to playerstart at the very start of game
            if(!firstFocusDone)
            {
                GameManager.GM.FocusToCurrentEntity();
                firstFocusDone = true;
            }
        }
        GameManager.GM.gridManager.getSpace(currentActingEntity.xPosInGrid, currentActingEntity.yPosInGrid).GetComponent<Renderer>().material.color = Color.magenta;
        GameManager.GM.FocusToCurrentEntity();
    }

    public bool CheckIfLose()
    {
        for (int i = 0; i < playerEntities.Count; i++)
        {
            if (!playerEntities[i].isDead)
                return false;
        }
        return true;
    }

    ///Does movement logic and checks if this specific character has already moved
    public void DoMove()
    {
        SoundManager.PlayASource("Button");
        if (!hasMoved && !GameManager.GM.playerCanMove && !currentActingEntity.isDead && GameManager.GM.canAct)
        {
            GameManager.GM.playerCanAttack = false;
            GameManager.GM.playerMeleeAttack = false;
            GameManager.GM.playerRangedAttack = false;
            GameManager.GM.playerMagicAttack = false;
            ScreenLog.PrintToLog("Player character: <color=#00ff00ff>" + currentActingEntity.eName + "</color> moving..");
            GameManager.GM.playerRoll = moveRoll;
            currentActingEntity.action.PerformActionPlayer(ActionType.Move);
            //GameManager.GM.currentMovingEntity = currentActingEntity;
        }
        else if(!hasMoved && GameManager.GM.playerCanMove && !currentActingEntity.isDead)
        {
            GameManager.GM.playerCanMove = false;
            ScreenLog.PrintToLog("Moving cancelled");
        }
    }

    public void DoAttack()
    {
        SoundManager.PlayASource("Button");
        if (!hasAttacked && !currentActingEntity.isDead && !GameManager.GM.playerCanAttack && GameManager.GM.canAct)
        {
            GameManager.GM.playerCanMove = false;
            currentActingEntity.action.PerformActionPlayer(ActionType.Attack);
        }
        else if(!hasAttacked && !currentActingEntity.isDead && GameManager.GM.playerCanAttack)
        {
            GameManager.GM.ResetNodes();
            GameManager.GM.playerCanAttack = false;
            GameManager.GM.playerMeleeAttack = false;
            GameManager.GM.playerRangedAttack = false;
            GameManager.GM.playerMagicAttack = false;
            ScreenLog.PrintToLog("Attack cancelled");
        }
    }
    //public void DoRangedAttack()
    //{
    //    if (!hasAttacked && !currentActingEntity.isDead && !GameManager.GM.playerCanAttack && GameManager.GM.canAct)
    //    {
    //        currentActingEntity.action.PerformActionPlayer(ActionType.Attack);
    //    }
    //    else if (!hasAttacked && !currentActingEntity.isDead && GameManager.GM.playerCanAttack)
    //    {
    //        GameManager.GM.ResetNodes();
    //        GameManager.GM.playerCanAttack = false;
    //        ScreenLog.PrintToLog("Attack cancelled");
    //    }
    //}
    //public void DoMagicAttack()
    //{
    //    if (!hasAttacked && !currentActingEntity.isDead && !GameManager.GM.playerCanAttack && GameManager.GM.canAct)
    //    {
    //        currentActingEntity.action.PerformActionPlayer(ActionType.Attack);
    //    }
    //    else if (!hasAttacked && !currentActingEntity.isDead && GameManager.GM.playerCanAttack)
    //    {
    //        GameManager.GM.ResetNodes();
    //        GameManager.GM.playerCanAttack = false;
    //        ScreenLog.PrintToLog("Attack cancelled");
    //    }
    //}

    ///End current player characters turn
    public void PassTurn()
    {
        SoundManager.PlayASource("Button");
        GameManager.GM.ResetNodes();
        if (!GameManager.GM.canAct)
            return;

        eIndex++;
        //here we need to check if its last player controlled character and then let ai play
        //otherwise we skip to our other characters
        if(GameManager.GM.currentTurnType == Turn.Player)
        {
            if (eIndex > playerEntities.Count - 1)
            {
                //decrease speedbuff
                if (currentActingEntity.speedPotionUptime > 0)
                    currentActingEntity.speedPotionUptime--;

                //decrease armorbuff
                if (currentActingEntity.stoneskinPotionUptime > 0)
                    currentActingEntity.stoneskinPotionUptime--;

                GameManager.GM.NextTurn();
                eIndex = 0;
            }
            else
            {
                //decrease speedbuff
                if (currentActingEntity.speedPotionUptime > 0)
                    currentActingEntity.speedPotionUptime--;

                //decrease armorbuff
                if (currentActingEntity.stoneskinPotionUptime > 0)
                    currentActingEntity.stoneskinPotionUptime--;

                DoTurn();
            }
        }
    }

	public static void addScrore(int amount)
    {
        score += amount;
        FindObjectOfType<PlayerController>().UpdateScoreText();
    }

    public void ActivateAttackPanel()
    {
        
    }
}
