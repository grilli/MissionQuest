﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[System.Serializable]
public class GridSpace : MonoBehaviour
{
    public float xScale, yScale;
    public int xPosition, yPosition;

    [HideInInspector]
    public GridSpace parent;
    [HideInInspector]
    public float f_score;
    [HideInInspector]
    public float G;
    [HideInInspector]
    public float H;
    [HideInInspector]
    public List<GridSpace> neighbors;

    public bool isWall = false;
    public bool canBeWalked = true;
    public bool hasEntity = false;
    public bool isEmpty = false;

    public bool isDoor = false;
    public GameObject doorObj;
    public GameObject wallObj;

    public bool isHidden = false;
    public GameObject hiddenFog;

    public bool isChest = false;
    public ChestType chestType;
    public Chest chest;

    public Color normal, highLight, selectedC;

    private bool selected = false;

    public Entity charOnTile = null;

    public bool attackSelect = false;

    public Vector2 getPosition()
    {
        return new Vector2(xPosition, yPosition);
    }

    private void Awake()
    {
        Vector3 scale = new Vector3(xScale, yScale, 0.5f );
        transform.localScale = scale;
    }

    public void MouseEnter()
    {
       //GameManager.GM.ResetNodes();
       GetComponent<MeshRenderer>().material.color = highLight;
       GameManager.GM.currentGridPosText.text = "(" + xPosition + ", " + yPosition + ")";
       GameManager.GM.gridManager.LastMOver = this;

       if(GameManager.GM.playerCanMove && GameManager.GM.canAct)
            GameManager.GM.gridManager.GetComponent<AStarPathfinding>().Pathfinding(GameManager.GM.currentMovingEntity.GetEntityPosition(), new Vector2(GameManager.GM.gridManager.LastMOver.xPosition, GameManager.GM.gridManager.LastMOver.yPosition), GameManager.GM.playerRoll, false);
       if(!FindObjectOfType<PlayerController>().hasAttacked && GameManager.GM.canAct && GameManager.GM.playerCanAttack && GameManager.GM.playerMagicAttack)
       {
            //List<GridSpace> path = GameManager.GM.gridManager.GetComponent<AStarPathfinding>().GetPath(GameManager.GM.currentMovingEntity.GetEntityPosition(), new Vector2(GameManager.GM.gridManager.LastMOver.xPosition, GameManager.GM.gridManager.LastMOver.yPosition));
            //for (int i = 0; i < path.Count; i++)
            //{
            //    if (i < GameManager.GM.currentMovingEntity.stats.attackRange)
            //        path[i].GetComponent<Renderer>().material.color = Color.green;
            //    else
            //        path[i].GetComponent<Renderer>().material.color = Color.red;
            //}

            GridManager grm = FindObjectOfType<GridManager>();
            List<GridSpace> neighbouringEntities = new List<GridSpace>();
            GridSpace vn;

            //one node down
            vn = grm.getSpace(this.xPosition, this.yPosition - 1);
                neighbouringEntities.Add(vn);
            //one node up
            vn = grm.getSpace(this.xPosition, this.yPosition + 1);
                neighbouringEntities.Add(vn);
            //one node to left
            vn = grm.getSpace(this.xPosition - 1, this.yPosition);
                neighbouringEntities.Add(vn);
            //one node to right
            vn = grm.getSpace(this.xPosition + 1, this.yPosition);
                neighbouringEntities.Add(vn);

            foreach (GridSpace g in neighbouringEntities)
            {
                if(g != null)
                    g.GetComponent<Renderer>().material.color = Color.blue;
            }
        }
    }

    public void MouseLeave()
    {
        if(selected)
            GetComponent<MeshRenderer>().material.color = selectedC;
        else
            GetComponent<MeshRenderer>().material.color = normal;
        if (GameManager.GM.canAct && GameManager.GM.playerCanAttack && GameManager.GM.playerMagicAttack)
        {
            GridManager grm = FindObjectOfType<GridManager>();
            List<GridSpace> neighbouringEntities = new List<GridSpace>();
            GridSpace vn;

            //one node down
            vn = grm.getSpace(this.xPosition, this.yPosition - 1);
            neighbouringEntities.Add(vn);
            //one node up
            vn = grm.getSpace(this.xPosition, this.yPosition + 1);
            neighbouringEntities.Add(vn);
            //one node to left
            vn = grm.getSpace(this.xPosition - 1, this.yPosition);
            neighbouringEntities.Add(vn);
            //one node to right
            vn = grm.getSpace(this.xPosition + 1, this.yPosition);
            neighbouringEntities.Add(vn);

            foreach (GridSpace g in neighbouringEntities)
            {
                if(g != null)
                    g.GetComponent<Renderer>().material.color = normal;
            }
        }
    }

    public void Select()
    {
        GetComponent<MeshRenderer>().material.color = selectedC;
        selected = true;
    }

    public void DeSelect()
    {
        GetComponent<MeshRenderer>().material.color = normal;
        selected = false;
        attackSelect = false;
    }

    void OnMouseExit()
    {
       if (GameManager.GM.playerCanMove && GameManager.GM.canAct)
            GameManager.GM.ResetNodes();
        MouseLeave();  
    }

    void OnMouseEnter()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
            MouseEnter();
    }

    public void OnMouseDown()
    {
        if (!EventSystem.current.IsPointerOverGameObject())
        {
            /// Movement
            // Checks if it's currently player turn and if player can move
            if (GameManager.GM.currentTurnType == Turn.Player && GameManager.GM.playerCanMove && canBeWalked && GameManager.GM.canAct)
            {
                Entity entity = GameManager.GM.currentMovingEntity;

                Vector2 oldPos = entity.GetEntityPosition();

                /*entity.xPosInGrid = (int)transform.position.x - 1;
                entity.yPosInGrid = (int)transform.position.z;*/

                //find path to clicked node
                GameManager.GM.gridManager.GetComponent<AStarPathfinding>().Pathfinding(oldPos, new Vector2(this.xPosition, this.yPosition), GameManager.GM.playerRoll, true);

                //GameManager.GM.currentMovingEntity.action.actionsLeft--;
            }

            /// Combat
            // Melee attack
            // Checks if it's currently player turn and if player can attack
            if (GameManager.GM.currentTurnType == Turn.Player && GameManager.GM.playerCanAttack && GameManager.GM.playerMeleeAttack && !FindObjectOfType<PlayerController>().hasAttacked && hasEntity && GameManager.GM.canAct)
            {
                Entity entity = GameManager.GM.currentMovingEntity;
                bool isNeighbour = false;
                List<GridSpace> gsl = GameManager.GM.gridManager.GetComponent<AStarPathfinding>().FindValidFourNeighbours(FindObjectOfType<GridManager>().getSpace(entity.xPosInGrid, entity.yPosInGrid));
                for (int i = 0; i < gsl.Count; i++)
                {
                    if (gsl[i] == this)
                        isNeighbour = true;
                }
                if (entity != charOnTile && isNeighbour)
                {
                    List<GridSpace> gs = new List<GridSpace>();
                    gs.Add(this);
                    GameManager.GM.gridManager.GetComponent<AStarPathfinding>().movementRotation(gs, 0);
                    entity.action.Attack(entity, charOnTile, true);
                    FindObjectOfType<PlayerController>().hasAttacked = true;
                    SoundManager.PlayASource("SwordHit");
                }
            }
            // Ranged attack
            else if (GameManager.GM.currentTurnType == Turn.Player && GameManager.GM.playerCanAttack && GameManager.GM.playerRangedAttack && !FindObjectOfType<PlayerController>().hasAttacked && hasEntity && GameManager.GM.canAct)
            {
                Entity entity = GameManager.GM.currentMovingEntity;

                // Checks if target tile is in range
                // TODO: use pathfinding instead
                if ((int)GameManager.GM.gridManager.getDistanceBetweenEntities(entity, this.charOnTile) <= entity.stats.attackRange)
                {
                    Debug.Log((int)GameManager.GM.gridManager.getDistanceBetweenEntities(entity, this.charOnTile));

                    if (entity != charOnTile)
                    {
                        entity.action.Attack(entity, charOnTile, false);
                        FindObjectOfType<PlayerController>().hasAttacked = true;
                        SoundManager.PlayASource("HunterAttack");
                    }
                }
            }
            // Magic attack
            else if (GameManager.GM.currentTurnType == Turn.Player && GameManager.GM.playerCanAttack && GameManager.GM.playerMagicAttack && !FindObjectOfType<PlayerController>().hasAttacked /*&& hasEntity*/ && GameManager.GM.canAct)
            {
                /// Note! This can be used to attack empty tiles! Be careful if you edit this!
                Entity entity = GameManager.GM.currentMovingEntity;

                // Checks if target tile is in range
                // TODO: use pathfinding instead
                if ((int)GameManager.GM.gridManager.getDistanceBetweenGridSpaces(GameManager.GM.gridManager.getSpace(entity.xPosInGrid,entity.yPosInGrid),this) <= entity.stats.attackRange)
                {
                    Debug.Log((int)GameManager.GM.gridManager.getDistanceBetweenGridSpaces(GameManager.GM.gridManager.getSpace(entity.xPosInGrid, entity.yPosInGrid), this));

                    List<GridSpace> gsl = new List<GridSpace>();
                    gsl = GameManager.GM.gridManager.GetComponent<AStarPathfinding>().FindValidFourNeighbours(this);
                    gsl.Add(this);

                    for (int i = 0; i < gsl.Count; i++)
                    {
                        if (gsl[i].hasEntity) // This is done to prevent errors from attacking empty tiles
                        {
                            entity.action.Attack(entity, gsl[i].charOnTile, false);
                            SoundManager.PlayASource("FireSpell");
                        }
                        GameObject.Instantiate(GameManager.GM.fireParticle, gsl[i].transform.position + Vector3.up * 2f, Quaternion.identity);
                    }
                    FindObjectOfType<PlayerController>().hasAttacked = true;
                }
            }
            //Can open chest by clicking NOT WORKING
            /*
            if (GameManager.GM.currentTurnType==Turn.Player && GameManager.GM.playerCanAttack && (!FindObjectOfType<PlayerController>().hasAttacked||!FindObjectOfType<PlayerController>().hasMoved) && isChest && GameManager.GM.canAct)
            {
                Entity entity = GameManager.GM.currentMovingEntity;
                entity.action.OpenChest(entity, this);
            }*/
        }   
    }

    public enum ChestType
    {
        Small,
        Medium,
        Large,
        Objective
    }
}
