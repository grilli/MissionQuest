﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public GameObject InventoryUI;
    public Sprite noItemSlot;

    public Item[] ItemsInInventory = new Item[16];
    public List<Button> slots = new List<Button>();

    private PlayerController PC;
    

    public void ShowInventory()
    {
        if (InventoryUI.activeSelf)       
            InventoryUI.SetActive(false);                
        else    
            InventoryUI.SetActive(true);

        SoundManager.PlayASource("Inventory");

    }

    private void Awake()
    {
        PC = FindObjectOfType<PlayerController>();
    }

    private void Update()
    {
        
        if(Input.GetKeyDown(KeyCode.Alpha1) && InventoryUI.activeSelf)
        {
            //FindObjectOfType<PlayerController>().inventory.AddItem(new StoneskinPotion(ItemDatabase.IDB.GetConsumableWithName("Stoneskin Potion")));
            /*FindObjectOfType<PlayerController>().inventory.AddItem(new Shield(ItemDatabase.IDB.GetShieldWithName("Buckler Shield")));
            FindObjectOfType<PlayerController>().inventory.AddItem(new Shield(ItemDatabase.IDB.GetShieldWithName("Kite Shield")));
            FindObjectOfType<PlayerController>().inventory.AddItem(new Armor(ItemDatabase.IDB.GetArmorWithName("Leather Armor")));
            FindObjectOfType<PlayerController>().inventory.AddItem(new Armor(ItemDatabase.IDB.GetArmorWithName("Chainmail Armor")));
            int roll = Random.Range(0, 6);
            switch(roll)
            {
                case 0:
                    AddItem(new Weapon(ItemDatabase.IDB.GetWeaponWithName("Master Sword")));
                    break;
                case 1:
                    AddItem(new HealthPotion(ItemDatabase.IDB.GetConsumableWithName("Health Potion"), 5));
                    break;
                case 2:
                    AddItem(new Armor(ItemDatabase.IDB.GetArmorWithName("Plate Armor")));
                    break;
                case 3:
                    AddItem(new Shield(ItemDatabase.IDB.GetShieldWithName("America")));
                    break;
                case 4:
                    AddItem(new Weapon(ItemDatabase.IDB.GetWeaponWithName("Stick")));
                    break;
                case 5:
                    AddItem(new Revive(ItemDatabase.IDB.GetConsumableWithName("Revive")));
                    break;*/
                    
            }
    }

    public Button FindEmptySlot()
    {
        foreach(Button b in slots)
        {
            if (b.image.sprite == noItemSlot)
                return b;
        }

        return null;
    }

    public void AddItem(Item item)
    {
        Button b = FindEmptySlot();

        if(b != null)
        {
            b.onClick.AddListener(delegate { UseItem(item, slots.IndexOf(b)); });
            b.image.sprite = item.image;
            ItemsInInventory[slots.IndexOf(b)] = item;
        }
    }

    public void RemoveItem(Item item, int index)
    {
        slots[index].image.sprite = noItemSlot;
        slots[index].onClick.RemoveAllListeners();

        ItemsInInventory[index].iName = "";
        ItemsInInventory[index].image = null;
    }

    public void UseItem(Item item, int index)
    {
        //dont do anything if its not players turn
        if (GameManager.GM.currentTurnType != Turn.Player)
            return;

        //we cant use items exept revive if we are dead
        if(!FindObjectOfType<PlayerController>().currentActingEntity.isDead || item.iName == "Revive Poultice")
        {
            item.UseItem();
            //we dont delete the key
            if(item.iName != "Key")
                RemoveItem(item, index);
        }
    }

    public void EquipItem(ItemType iType, Item item)
    {
        switch(iType)
        {
            case ItemType.Armor:
                Armor a = new Armor((Armor)item);
                PC.currentActingEntity.stats.AdjustStats(0, a.defenceDiceIncrease, a.healthIncrease, a.slowAmount, ItemType.Armor);
                PC.currentActingEntity.stats.armor = a;
                break;
            case ItemType.Weapon:
                Weapon w = new Weapon((Weapon)item);
                PC.currentActingEntity.stats.AdjustStats(w.attackDiceIncrease, 0, 0, 0, ItemType.Weapon);
                PC.currentActingEntity.stats.weapon = w;
                break;
            case ItemType.Shield:
                Shield s = new Shield((Shield)item);
                PC.currentActingEntity.stats.AdjustStats(0, s.defenceDiceIncrease, 0, s.slowAmount, ItemType.Shield);
                PC.currentActingEntity.stats.shield = s;
                break;
            default:
                //do nothing as its not equippable armor ( maybe tell the player that his retard )
                break;
        }
    }

}
